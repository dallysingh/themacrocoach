<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('login', ['uses' => 'LoginController@viewLogin']);

Route::get('impersonate', ['uses' => 'LoginController@viewImpersonate']);
Route::post('impersonate', ['uses' => 'LoginController@processImpersonate']);

Route::get('sign-up', ['uses' => 'LoginController@viewSignUp']);
Route::post('sign-up', ['uses' => 'LoginController@processSignUp']);

Route::get('/', ['uses' => 'DashboardController@viewDashboard'])->middleware('auth');
Route::get('/home', 'DashboardController@viewDashboard')->middleware('auth');
Route::get('signature', ['uses' => 'DashboardController@viewSignature']);

Route::get('get-my-foods', ['uses' => 'MyFoodsController@getMyFoods'])->middleware('auth');
Route::get('get-my-meals', ['uses' => 'MyMealsController@getMyMeals'])->middleware('auth');

Route::post('save-day', ['uses' => 'MyDaysController@saveDay'])->middleware('auth');
Route::post('delete-day', ['uses' => 'MyDaysController@deleteDay'])->middleware('auth');

Route::get('my-days', ['uses' => 'MyDaysController@viewMyDays'])->middleware('auth');

Route::get('my-foods', ['uses' => 'MyFoodsController@viewMyFoods'])->middleware('auth');
Route::post('add-food', ['uses' => 'MyFoodsController@addFood'])->middleware('auth');
Route::post('update-food', ['uses' => 'MyFoodsController@updateFood'])->middleware('auth');
Route::post('delete-food', ['uses' => 'MyFoodsController@deleteFood'])->middleware('auth');

Route::get('meal-ideas', ['uses' => 'MealIdeasController@viewMealIdeas'])->middleware('auth');
Route::post('create-meal-idea', ['uses' => 'MealIdeasController@createMealIdea'])->middleware('auth');
Route::get('get-foods', ['uses' => 'FoodsDatabaseController@getFoods'])->middleware('auth');

Route::get('my-weight', ['uses' => 'MyWeightController@viewMyWeight'])->middleware('auth');
Route::get('get-my-weight', ['uses' => 'MyWeightController@getMyWeight'])->middleware('auth');
Route::get('get-my-current-weight', ['uses' => 'MyWeightController@getMyCurrentWeight'])->middleware('auth');
Route::post('set-weight-goal', ['uses' => 'MyWeightController@setWeightGoal'])->middleware('auth');
Route::post('add-weight', ['uses' => 'MyWeightController@addWeight'])->middleware('auth');
Route::post('edit-weight', ['uses' => 'MyWeightController@editWeight'])->middleware('auth');
Route::post('delete-weight', ['uses' => 'MyWeightController@deleteWeight'])->middleware('auth');

Route::get('get-my-profile', ['uses' => 'MyProfileController@getMyProfile'])->middleware('auth');

Route::get('foods-database', ['uses' => 'FoodsDatabaseController@viewFoodsDatabase'])->middleware('auth');
Route::post('add-food-db', ['uses' => 'FoodsDatabaseController@addFoodDb'])->middleware('auth');

Route::get('get-activity', ['uses' => 'DashboardController@getActivity'])->middleware('auth');
Route::post('set-activity', ['uses' => 'DashboardController@setActivity'])->middleware('auth');
Route::post('set-calorie-intake', ['uses' => 'DashboardController@setCalorieIntake'])->middleware('auth');

Route::get('my-profile', ['uses' => 'MyProfileController@viewMyProfile'])->middleware('auth');
Route::post('update-profile', ['uses' => 'MyProfileController@updateProfile'])->middleware('auth');

Route::get('my-training', ['uses' => 'MyTrainingController@viewMyTraining'])->middleware('auth');

Auth::routes();
