<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class MyDays extends Model {

	public static function saveDay($day_name, $day_id, $meals, $duplicate)
	{
		if($day_id != '-' && $duplicate == 0)
		{
			DB::table('days')->where('id', '=', $day_id)->delete();
		}

		$day_id = DB::table('days')->insertGetId(
		    			['user_id' => Auth::user()->id, 'day_name' => $day_name]
		);

			foreach ($meals as $meal)
			{
				DB::table('meals')->insertGetId(
				    	 ['day_id' => $day_id,
				    	  'user_id' => Auth::user()->id,
				    	  'meal_number' => $meal['meal_number'],
				    	  'food_id' => $meal['food_id'],
				    	  'weight' => $meal['per'],
				    	  'unit' => $meal['unit'],
				    	  'protein' => $meal['protein'],
				    	  'carbs' => $meal['carbs'],
				    	  'fat' => $meal['fat'],
				    	  'calories' => $meal['calories']
				    	 ]
				);
			}
	}

	public static function deleteDay($day_id)
	{
		DB::table('days')->where('id', $day_id)->delete();
	}

	public static function getMyDays()
	{
		$user = Auth::user();

		$my_days = DB::table('days')->orderBy('day_name', 'asc')
									->where('user_id', Auth::user()->id)
									->get();

		return $my_days;
	}

}