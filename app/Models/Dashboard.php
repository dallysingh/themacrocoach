<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Dashboard extends Model {

	public static function setActivity($activity_level, $activity_title, $activity_description, $maintenance_calories)
	{
		DB::table('activity_statistics')->where('user_id', Auth::user()->id)
		    			 			 	->update(['activity_level' => $activity_level,
		    			 			 		      'activity_title' => $activity_title,
		    			 			 		      'activity_description' => $activity_description,
		    			 			 		      'maintenance_calories' => $maintenance_calories]);
	}

	public static function setCalorieIntake($calorie_intake)
	{
		DB::table('activity_statistics')->where('user_id', Auth::user()->id)
		    			 			 	->update(['calorie_intake' => $calorie_intake]);
	}

	public static function getActivityStatistics()
	{
		$activity_statistics = DB::table('activity_statistics')
									->where('user_id', Auth::user()->id)
									->get();

		return $activity_statistics;
	}

}