<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class MyMeals extends Model {

	public static function getMyMeals()
	{
		$meals = DB::select(DB::raw('SELECT m.user_id, m.day_id, m.meal_number, m.food_id, f.food_name, m.weight, m.unit, m.protein, m.carbs, m.fat, m.calories
									 FROM meals m
									 JOIN foods f
								     ON m.food_id = f.id
								     WHERE m.user_id = ?'), array(Auth::user()->id));

        return $meals;
	}

	public static function getFoodinMeal($food_id)
	{
		$food = DB::table('meals')->where('user_id', Auth::user()->id)
								  ->where('food_id', $food_id)
								  ->get();

		return $food;
	}

}