<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class FoodsDatabase extends Model {

	public static function getFoods()
	{
		$foods = DB::table('foods_global')->orderBy('food_name', 'asc')
								          ->get();

		return $foods;
	}

	public static function getProteinFoods()
	{
		$foods = DB::table('foods_global')->orderBy('food_name', 'asc')
								   ->where('macro', 'protein')
								   ->get();

		return $foods;
	}

	public static function getCarbFoods()
	{
		$foods = DB::table('foods_global')->orderBy('food_name', 'asc')
								   ->where('macro', 'carbs')
								   ->get();

		return $foods;
	}

	public static function getFatFoods()
	{
		$foods = DB::table('foods_global')->orderBy('food_name', 'asc')
								   ->where('macro', 'fat')
								   ->get();

		return $foods;
	}

	public static function createMealIdea($protein_id, $carb_id, $fat_id)
	{
		$foods = DB::table('foods_global')->orderBy('macro', 'asc')
								   ->where('id', $protein_id)
								   ->orWhere('id', $carb_id)
								   ->orWhere('id', $fat_id)
								   ->get();

		return $foods;
	}

	public static function addFoodDb($food_id, $food_name, $per, $unit, $protein, $carbs, $fat, $calories)
	{
		DB::table('foods')->insertGetId(
		    			['user_id' => Auth::user()->id,
		    			 'global_id' => $food_id,
		    			 'food_name' => $food_name,
		    			 'per' => $per,
		    			 'unit' => $unit,
		    			 'protein' => $protein,
		    			 'carbs' => $carbs,
		    			 'fat' => $fat,
		    			 'calories' => $calories]);
	}
}