<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class MyProfile extends Model {

	public static function getMyProfile()
	{
		$profile = DB::table('profiles')->where('user_id', Auth::user()->id)
										->get();

		return $profile;
	}

	public static function updateProfile($first_name, $surname, $email, $dob, $gender, $height_cm, $height_ft, $height_inch, $bmr, $maintenance_calories)
	{
		DB::table('users')
            ->where('id', Auth::user()->id)
            ->update(['first_name' => $first_name, 'surname' => $surname, 'email' => $email]);

        DB::table('profiles')
            ->where('user_id', Auth::user()->id)
            ->update(['dob' => $dob, 'gender' => $gender, 'height_cm' => $height_cm, "height_ft" => $height_ft, "height_inch" => $height_inch]);

        DB::table('activity_statistics')
            ->where('user_id', Auth::user()->id)
            ->update(['maintenance_calories' => $maintenance_calories]);
     	
     	DB::select(DB::raw('UPDATE weight
							SET bmr = ?
							WHERE user_id = ?
							ORDER BY id DESC
							LIMIT 1'), array($bmr, Auth::user()->id));
            
	}

}