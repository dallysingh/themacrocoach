<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class MyFoods extends Model {

	public static function getMyFoods()
	{
		$my_foods = DB::table('foods')->orderBy('food_name', 'asc')
									  ->where('user_id', Auth::user()->id)
									  ->get();

		return $my_foods;
	}

	public static function addFood($food_name, $per, $unit, $protein, $carbs, $fat, $calories)
	{
		DB::table('foods')->insertGetId(
		    			['user_id' => Auth::user()->id,
		    			 'food_name' => $food_name,
		    			 'per' => $per,
		    			 'unit' => $unit,
		    			 'protein' => $protein,
		    			 'carbs' => $carbs,
		    			 'fat' => $fat,
		    			 'calories' => $calories]);
	}

	public static function getFood($food_id)
	{
		$food = DB::table('foods')->where('user_id', Auth::user()->id)
								  ->where('id', $food_id)
								  ->get();

		return $food;
	}

	public static function updateFood($food_id, $food_name, $per, $unit, $protein, $carbs, $fat, $calories)
	{
		DB::table('foods')->where('id', $food_id)
		    			  ->update(['food_name' => $food_name,
		    			 			'per' => $per,
		    			 			'unit' => $unit,
		    			 			'protein' => $protein,
		    			 			'carbs' => $carbs,
		    			 			'fat' => $fat,
		    			 			'calories' => $calories]);

		$food = MyMeals::getFoodinMeal($food_id);

		foreach($food as $item)
		{
			$weight = $item->weight;

			$meal_protein = round($weight * ($protein / $per));
			$meal_carbs = round($weight * ($carbs / $per));
			$meal_fat = round($weight * ($fat / $per));

			$meal_calories = ($meal_protein * 4) + ($meal_carbs * 4) + ($meal_fat * 9);

			DB::table('meals')->where('id', $item->id)
							  ->where('food_id', $food_id)
			    			  ->update(['protein' => $meal_protein,
			    			 			'carbs' => $meal_carbs,
			    			 			'fat' => $meal_fat,
			    			 			'calories' => $meal_calories]);
		}
	}

	public static function deleteFood($food_id)
	{
		DB::table('foods')->where('id', $food_id)->delete();
	}

}