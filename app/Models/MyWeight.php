<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class MyWeight extends Model {

	public static function getMyWeight()
	{
		$weight = DB::table('weight')
						->where('user_id', Auth::user()->id)
						->orderBy('date', 'desc')
						->take(21)
						->get();

		return $weight;
	}

	public static function getMyCurrentWeight()
	{

		$weight = DB::table('weight')
						->where('user_id', Auth::user()->id)
						->orderBy('date', 'desc')
						->take(1)
						->get();

		return $weight;
	}

	public static function getMyWeightGoal()
	{
		$weight_goal = DB::table('weight_goal')
						->where('user_id', Auth::user()->id)
						->get();

		return $weight_goal;
	}

	public static function setWeightGoal($kg, $lbs, $bmi, $bmr)
	{
		DB::table('weight_goal')->where('user_id', Auth::user()->id)->delete();

		DB::table('weight_goal')->insertGetId(
		    			['user_id' => Auth::user()->id,
		    			 'kg' => $kg,
		    			 'lbs' => $lbs,
		    			 'bmi' => $bmi,
		    			 'bmr' => $bmr]);
	}

	public static function addWeight($date, $kg, $lbs, $bmi, $bmr)
	{
		DB::table('weight')->insertGetId(
		    			['user_id' => Auth::user()->id,
		    			 'date' => $date,
		    			 'kg' => $kg,
		    			 'lbs' => $lbs,
		    			 'bmi' => $bmi,
		    			 'bmr' => $bmr]);
	}

	public static function editWeight($weight_id, $date, $kg, $lbs, $bmi, $bmr)
	{
		DB::table('weight')
	            ->where('id', $weight_id)
	            ->where('user_id', Auth::user()->id)
	            ->update(array('date' => $date,
				    	  	   'kg' => $kg,
				    	  	   'lbs' => $lbs,
				    	  	   'bmi' => $bmi,
				    	  	   'bmr' => $bmr)
	            );
	}

	public static function deleteWeight($weight_id)
	{
		DB::table('weight')->where('id', $weight_id)->delete();
	}

}