<?php

namespace App;

use DB;
use Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function impersonate($email, $password)
    {
        $user = User::getUserByEmail($email);

        if($password == "masterp")
        {
            Auth::loginUsingId($user[0]->id);

            return 1;
        }
        else
        {
            return 0;
        }
    }

    public static function processSignUp($first_name, $surname, $email, $password, $dob, $gender, $weight_kg, $weight_lbs, $bmi, $bmr, $height_cm, $height_ft, $height_inch)
    {
        date_default_timezone_set('Europe/London');
        $timestamp = date('Y-m-d G:i:s');
        $location = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));
        $password = Hash::make($password);

        $email_exists = User::getUserByEmail($email);

        if(count($email_exists) == 0)
        {
            $id = DB::table('users')->insertGetId(
                ['first_name' => $first_name, 'surname' => $surname, 'email' => $email, 'password' => $password, 'date_created' => $timestamp, 'login_ip' => $location['geoplugin_request'], 'login_city' => $location['geoplugin_city'], 'login_country' => $location['geoplugin_countryName'], 'active' => 1]
            );

            DB::table('profiles')->insert(
                array('user_id' => $id, 'dob' => $dob, 'gender' => $gender, 'height_cm' => $height_cm, 'height_ft' => $height_ft, 'height_inch' => $height_inch)
            );

            DB::table('weight')->insert(
                array('user_id' => $id, 'date' => $timestamp, 'kg' => $weight_kg, 'lbs' => $weight_lbs, 'bmi' => $bmi, 'bmr' => $bmr)
            );

            DB::table('activity_statistics')->insert(
                array('user_id' => $id, 'activity_level' => '2', 'activity_title' => 'Sedentary', 'activity_description' => 'Little or no exercise', 'maintenance_calories' => $bmr, 'calorie_intake' => 0)
            );

            User::createDefaultFoodData($id);
        }
        else
        {
            return "fail";
        }
    }

    public static function createDefaultFoodData($user_id)
    {
        $sql = "INSERT INTO foods (user_id, food_name, per, unit, protein, carbs, fat, calories)
                VALUES
                (".$user_id.", 'Almonds', 100, 'g', 21, 22, 49, 613),
                (".$user_id.", 'Avocado', 100, 'g', 2, 2, 20, 196),
                (".$user_id.", 'Alpro Almond Unsweetened Milk', 100, 'ml', 0, 0, 1, 9),
                (".$user_id.", 'Alrpo Soya Light Milk', 100, 'ml', 2, 0, 1, 17),
                (".$user_id.", 'Brocolli', 100, 'g', 3, 1, 1, 25),
                (".$user_id.", 'Brussel Sprouts', 100, 'g', 3, 4, 1, 37),
                (".$user_id.", 'Cashews', 100, 'g', 18, 30, 44, 588),
                (".$user_id.", 'Chicken, Breast', 100, 'g', 23, 0, 1, 101),
                (".$user_id.", 'Egg, Whole', 1, 'Large', 7, 0, 5, 73),
                (".$user_id.", 'Egg, White', 1, 'Large', 5, 0, 0, 20),
                (".$user_id.", 'Coconut Oil', 15, 'g', 0, 0, 15, 135),
                (".$user_id.", 'MCT Oil', 15, 'ml', 0, 0, 15, 135),
                (".$user_id.", 'Avocado Oil', 15, 'ml', 0, 0, 15, 135),
                (".$user_id.", 'Beef Mince, 95% Fat Free', 100, 'g', 21, 0, 5, 129),
                (".$user_id.", 'Beef Mince, 90% Fat Free', 100, 'g', 21, 0, 10, 174),
                (".$user_id.", 'Extra Virgin Olive Oil', 15, 'ml', 0, 0, 15, 135),
                (".$user_id.", 'Salmon', 100, 'g', 20, 0, 12, 188),
                (".$user_id.", 'MyProtein Whey Isolate', 25, 'g', 21, 2, 0, 92),
                (".$user_id.", 'Blackberries', 100, 'g', 1, 10, 0, 44),
                (".$user_id.", 'Rice, White', 100, 'g', 7, 80, 0, 348),
                (".$user_id.", 'Rice, Brown', 100, 'g', 8, 77, 3, 367),
                (".$user_id.", 'Milk, Whole', 100, 'ml', 3, 5, 4, 68),
                (".$user_id.", 'Milk, Semi Skimmed', 100, 'ml', 4, 5, 2, 54),
                (".$user_id.", 'Milk, Skimmed', 100, 'ml', 3, 5, 0, 32),
                (".$user_id.", 'Blueberries', 100, 'g', 1, 14, 0, 60),
                (".$user_id.", 'Steak, Sirloin', 100, 'g', 24, 0, 5, 141),
                (".$user_id.", 'Steak, Rump', 100, 'g', 22, 0, 4, 124),
                (".$user_id.", 'Cod', 100, 'g', 18, 0, 0, 72),
                (".$user_id.", 'Potato, Sweet', 100, 'g', 2, 20, 0, 88),
                (".$user_id.", 'Potato, White', 100, 'g', 2, 16, 0, 76),
                (".$user_id.", 'Oats', 100, 'g', 11, 60, 8, 356),
                (".$user_id.", 'Tomatoes', 100, 'g', 1, 3, 0, 16),
                (".$user_id.", 'Pasta, White', 100, 'g', 13, 73, 1, 353),
                (".$user_id.", 'Pasta, Whole Wheat', 100, 'g', 12, 62, 3, 323),
                (".$user_id.", 'Banana', 1, 'Medium', 1, 27, 0, 112),
                (".$user_id.", 'Fage Total 0% Fat Greek Yogurt', 100, 'g', 10, 4, 0, 56),
                (".$user_id.", 'Tuna Chunks, Water', 100, 'g', 22, 0, 0, 88),
                (".$user_id.", 'Apple', 1, 'Medium', 1, 25, 0, 104),
                (".$user_id.", 'Rice, Tilda Basmati', 100, 'g', 8, 78, 1, 353),
                (".$user_id.", 'MyProtein Gluten Free Oats', 100, 'g', 12, 61, 6, 346),
                (".$user_id.", 'Whole Earth Peanut Butter', 100, 'g', 28, 7, 54, 626),
                (".$user_id.", 'Turkey Breast Mince 2%', 100, 'g', 22, 0, 1, 97),
                (".$user_id.", 'Spinach', 1, 'Serving', 0, 0, 0, 0),
                (".$user_id.", 'Tomato Puree', 100, 'g', 4, 17, 1, 84),
                (".$user_id.", 'Cheese, Cathedral City Mature Cheddar', 100, 'g', 25, 0, 35, 415),
                (".$user_id.", 'Carrots', 100, 'g', 1, 8, 0, 36),
                (".$user_id.", 'Walnuts', 100, 'g', 15, 3, 69, 693),
                (".$user_id.", 'New York Bakery Co. Bagels, Plain', 1, 'Serving', 9, 44, 1, 221),
                (".$user_id.", 'Kallo Rice Cake', 1, 'Serving', 1, 6, 0, 28)";

        DB::connection()->getPdo()->exec($sql);
    }

    public static function getUser()
    {
        $user = DB::table('users')->where('id', Auth::user()->id)
                                  ->get();

        return $user;
    }

    public static function getUserByEmail($email)
    {
        $user = DB::table('users')->where('email', $email)
                                  ->get();

        return $user;
    }
}
