<?php namespace App\Http\Controllers;

namespace App\Http\Controllers;

use View;
use Illuminate\Http\Request;

use App\Models\MyFoods;
use App\User;

class LoginController extends Controller {

	public function viewLogin()
	{
		return View::make('login');
	}

	public function viewImpersonate()
	{
		return View::make('impersonate');
	}

	public function viewSignUp()
	{
		return View::make('sign_up');
	}

	public function processImpersonate(Request $request)
	{
		$email = $request->input('email');
		$password = $request->input('password');

		$result = User::impersonate($email, $password);

		if($result == 1)
		{
			return redirect('/');
		}
		else
		{
			return redirect('impersonate');
		}
	}

	public function processSignUp()
	{
		$first_name = $_POST['first_name'];
		$surname = $_POST['surname'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		$dob = str_replace("/", "-", $_POST['dob']);
		$dob = date("Y-m-d", strtotime($dob));
		$gender = $_POST['gender'];
		$weight_kg = $_POST['weight_kg'];
		$weight_lbs = $_POST['weight_lbs'];
		$bmi = $_POST['bmi'];
		$bmr = $_POST['bmr'];
		$height_cm = $_POST['height_cm'];
		$height_ft = $_POST['height_ft'];
		$height_inch = $_POST['height_inch'];

		$result = User::processSignUp($first_name, $surname, $email, $password, $dob, $gender, $weight_kg, $weight_lbs, $bmi, $bmr, $height_cm, $height_ft, $height_inch);

		return $result;
	}

}