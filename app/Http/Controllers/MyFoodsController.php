<?php namespace App\Http\Controllers;

use View;
use App\Models\MyFoods;
use App\Models\FoodsDatabase;

class MyFoodsController extends Controller {

	public function getMyFoods()
	{
		$my_foods = MyFoods::getMyFoods();

		return $my_foods;
	}

	public function viewMyFoods()
	{
		$my_foods = MyFoods::getMyFoods();

		return View::make('my_foods')->with('my_foods', $my_foods);
	}

	public function addFood()
	{
		$food_name = $_POST['food_name'];
		$per = $_POST['per'];
		$unit = $_POST['unit'];
		$protein = $_POST['protein'];
		$carbs = $_POST['carbs'];
		$fat = $_POST['fat'];
		$calories = $_POST['calories'];

		MyFoods::addFood($food_name, $per, $unit, $protein, $carbs, $fat, $calories);
	}

	public function updateFood()
	{
		$food_id = $_POST['food_id'];
		$food_name = $_POST['food_name'];
		$per = $_POST['per'];
		$unit = $_POST['unit'];
		$protein = $_POST['protein'];
		$carbs = $_POST['carbs'];
		$fat = $_POST['fat'];
		$calories = $_POST['calories'];

		MyFoods::updateFood($food_id, $food_name, $per, $unit, $protein, $carbs, $fat, $calories);
	}

	public function deleteFood()
	{
		$food_id = $_POST['food_id'];
		MyFoods::deleteFood($food_id);
	}

}