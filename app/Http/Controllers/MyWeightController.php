<?php namespace App\Http\Controllers;

use View;
use App\Models\MyWeight;

class MyWeightController extends Controller {

	public function viewMyWeight()
	{
		$weight = MyWeight::getMyWeight();
		$current_weight = MyWeight::getMyCurrentWeight();
		$weight_goal = MyWeight::getMyWeightGoal();

		return View::make('my_weight')->with('weight', $weight)
									  ->with('current_weight', $current_weight)
									  ->with('weight_goal', $weight_goal);
	}

	public function getMyWeight()
	{
		$weight = MyWeight::getMyWeight();

		return $weight;
	}

	public function getMyCurrentWeight()
	{
		$weight = MyWeight::getMyCurrentWeight();

		return $weight;
	}

	public function setWeightGoal()
	{
		$kg = $_POST['kg'];
		$lbs = $_POST['lbs'];
		$bmi = $_POST['bmi'];
		$bmr = $_POST['bmr'];

		MyWeight::setWeightGoal($kg, $lbs, $bmi, $bmr);
	}

	public function addWeight()
	{
		$date = str_replace("/", "-", $_POST['date']);
		$date = date("Y-m-d", strtotime($date));
		$kg = $_POST['kg'];
		$lbs = $_POST['lbs'];
		$bmi = $_POST['bmi'];
		$bmr = $_POST['bmr'];

		MyWeight::addWeight($date, $kg, $lbs, $bmi, $bmr);
	}

	public function editWeight()
	{
		$weight_id = $_POST['weight_id'];
		$date = str_replace("/", "-", $_POST['date']);
		$date = date("Y-m-d", strtotime($date));
		$kg = $_POST['kg'];
		$lbs = $_POST['lbs'];
		$bmi = $_POST['bmi'];
		$bmr = $_POST['bmr'];

		MyWeight::editWeight($weight_id, $date, $kg, $lbs, $bmi, $bmr);
	}

	public function deleteWeight()
	{
		$weight_id = $_POST['weight_id'];

		MyWeight::deleteWeight($weight_id);
	}

}