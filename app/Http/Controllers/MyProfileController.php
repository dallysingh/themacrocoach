<?php namespace App\Http\Controllers;

use View;
use App\Models\MyProfile;
use App\User;

class MyProfileController extends Controller {

	public function viewMyProfile()
	{
		$profile = MyProfile::getMyProfile();
		$dob = $profile[0]->dob;
		$dob = date("d/m/Y", strtotime($dob));
		$user = User::getUser();

		return View::make('my_profile')->with('profile', $profile)
									   ->with('dob', $dob)
									   ->with('user', $user);
	}

	public function getMyProfile()
	{
		$profile = MyProfile::getMyProfile();

		return $profile;
	}

	public function updateProfile()
	{
		$first_name = $_POST['first_name'];
		$surname = $_POST['surname'];
		$email = $_POST['email'];
		$dob = str_replace("/", "-", $_POST['dob']);
		$dob = date("Y-m-d", strtotime($dob));
		$gender = $_POST['gender'];
		$height_cm = $_POST['height_cm'];
		$height_ft = $_POST['height_ft'];
		$height_inch = $_POST['height_inch'];
		$bmr = $_POST['bmr'];
		$maintenance_calories = $_POST['maintenance_calories'];

		MyProfile::updateProfile($first_name, $surname, $email, $dob, $gender, $height_cm, $height_ft, $height_inch, $bmr, $maintenance_calories);
	}

}