<?php namespace App\Http\Controllers;

use View;

class MyTrainingController extends Controller {

	public function viewMyTraining()
	{
		return View::make('my_training');
	}

}