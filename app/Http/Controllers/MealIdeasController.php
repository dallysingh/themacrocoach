<?php namespace App\Http\Controllers;

use View;
use App\Models\MyFoods;
use App\Models\FoodsDatabase;

class MealIdeasController extends Controller {

	public function viewMealIdeas()
	{
		$protein_foods = FoodsDatabase::getProteinFoods();
		$carb_foods = FoodsDatabase::getCarbFoods();
		$fat_foods = FoodsDatabase::getFatFoods();
		
		return View::make('meal_ideas')->with('protein_foods', $protein_foods)
									   ->with('carb_foods', $carb_foods)
									   ->with('fat_foods', $fat_foods);
	}

	public function createMealIdea()
	{
		$protein_id = $_POST['protein_id'];
		$carb_id = $_POST['carb_id'];
		$fat_id = $_POST['fat_id'];

		$foods = FoodsDatabase::createMealIdea($protein_id, $carb_id, $fat_id);
		return $foods;
	}

}