<?php namespace App\Http\Controllers;

use View;
use App\Models\MyMeals;

class MyMealsController extends Controller {

	public function getMyMeals()
	{
		$my_meals = MyMeals::getMyMeals();

		return $my_meals;
	}

}