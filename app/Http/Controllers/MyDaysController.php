<?php namespace App\Http\Controllers;

use View;
use App\Models\MyDays;
use App\Models\MyMeals;
use App\Models\Dashboard;

class MyDaysController extends Controller {

	public function saveDay()
	{
		$day_name = $_POST['day_name'];
		$day_id = $_POST['day_id'];
		$meals = $_POST['meals'];
		$duplicate = $_POST['duplicate'];

		MyDays::saveDay($day_name, $day_id, $meals, $duplicate);
	}

	public function deleteDay()
	{
		$day_id = $_POST['day_id'];

		MyDays::deleteDay($day_id);
	}

	public function viewMyDays()
	{
		$my_days = MyDays::getMyDays();
		$my_meals = MyMeals::getMyMeals();
		$activity = Dashboard::getActivityStatistics();

		return View::make('my_days')->with('my_days', $my_days)
									->with('my_meals', $my_meals)
									->with('activity', $activity);
	}

}