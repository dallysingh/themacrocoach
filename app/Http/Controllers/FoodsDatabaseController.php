<?php namespace App\Http\Controllers;

use View;
use App\Models\MyFoods;
use App\Models\FoodsDatabase;

class FoodsDatabaseController extends Controller {

	public function viewFoodsDatabase()
	{
		$my_foods = MyFoods::getMyFoods();
		$food_ids = array();

		foreach($my_foods as $food)
		{
			array_push($food_ids, $food->global_id);
		}

		$foods = FoodsDatabase::getFoods();

		return View::make('foods_database')->with('foods', $foods)
										   ->with('food_ids', $food_ids);
	}

	public function getFoods()
	{
		$foods = FoodsDatabase::getFoods();

		return $foods;
	}

	public function addFoodDb()
	{
		$food_id = $_POST['food_id'];
		$food_name = $_POST['food_name'];
		$per = $_POST['per'];
		$unit = $_POST['unit'];
		$protein = $_POST['protein'];
		$carbs = $_POST['carbs'];
		$fat = $_POST['fat'];
		$calories = $_POST['calories'];

		FoodsDatabase::addFoodDb($food_id, $food_name, $per, $unit, $protein, $carbs, $fat, $calories);
	}

}