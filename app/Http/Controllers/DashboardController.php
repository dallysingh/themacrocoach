<?php namespace App\Http\Controllers;

use View;
use App\Models\Dashboard;
use App\Models\MyWeight;

class DashboardController extends Controller {

	public function viewDashboard()
	{
		$my_weight = MyWeight::getMyCurrentWeight();
		$activity_statistics = Dashboard::getActivityStatistics();

		return View::make('dashboard')->with('my_weight', $my_weight)
									  ->with('activity_statistics', $activity_statistics);
	}

	public function getActivity()
	{
		$activity_statistics = Dashboard::getActivityStatistics();

		return $activity_statistics;
	}
	

	public function setActivity()
	{
		$activity_level = $_POST['activity_level'];
		$activity_title = $_POST['activity_title'];
		$activity_description = $_POST['activity_description'];
		$maintenance_calories = $_POST['maintenance_calories'];

		Dashboard::setActivity($activity_level, $activity_title, $activity_description, $maintenance_calories);
	}

	public function setCalorieIntake()
	{
		$calorie_intake = $_POST['calorie_intake'];

		Dashboard::setCalorieIntake($calorie_intake);
	}

	public function viewSignature()
	{
		return View::make('signature');
	}

}