<!DOCTYPE html>
<html lang="en">

@include('head')

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

  <script src="/js/bootstrap-datepicker.js"></script>

  <title>The Macro Coach - Sign Up</title>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body id="sign-up-page">

  <a href="/"><div id="logo"></div></a>

  <div id="signup-errors">
    
  </div>

  <div class="container">
    <div class="row">
      <div class="col-sm-12">

          <div id="sign-up-container">
            <form action="sign-up" method="post" id="sign-up-form">
              <input type="text" class="form-control check-field" name="first-name" id="first-name" placeholder="First Name" autocomplete="off">
              <input type="text" class="form-control check-field" name="surname" id="surname" placeholder="Surname" autocomplete="off">
              <input type="email" class="form-control check-field" name="email" id="email" placeholder="Email Address" autocomplete="off">
              <input type="password" class="form-control check-field" name="password" id="password" placeholder="Password">
              <input type="password" class="form-control check-field" name="confirm-password" id="confirm-password" placeholder="Confirm Password">
              <input type="text" data-provide="datepicker" class="form-control check-field datepicker" id="dob" placeholder="Date of Birth" autocomplete="off">
              <select id="gender" class="form-control">
                <option value="male">Male</option>
                <option value="female">Female</option>
              </select>

              <div class="input-container">
                <input type="text" class="form-control check-field" name="weight-kg" id="weight-kg" placeholder="kg" maxlength="5" autocomplete="off">
                <input type="text" class="form-control check-field" name="weight-lbs" id="weight-lbs" placeholder="lbs" maxlength="5" autocomplete="off">
              </div>

               <div class="input-container">
                <input type="text" class="form-control check-field" name="height-cm" id="height-cm" placeholder="cm" maxlength="3" autocomplete="off">
                <input type="text" class="form-control check-field" name="heigh-ft" id="height-ft" placeholder="ft" maxlength="1" autocomplete="off">
                <input type="text" class="form-control check-field" name="height-inch" id="height-inch" placeholder='"' maxlength="2" autocomplete="off">
               </div>

               <button type="submit" class="btn btn-default btn-block btn-primary" id="login-btn">Sign Up</button>
            </form>

            </form>
          </div>

      </div>
    </div>
  </div>

  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="/js/core.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/sign_up.js"></script>
</body>

</html>