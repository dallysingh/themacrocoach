<!DOCTYPE html>
<html lang="en">

@include('head')

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>The Macro Coach - Meal Ideas</title>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>

  @include('navigation')

  <div class="container">
    <div class="row">

      <div class="col-md-4"></div>
      <div class="col-md-4">

        <h4 id="meal-ideas-tagline">Choose your calories, pick your foods.</h4>
        <input type="range" min="100" max="1000" step="50" value="300" data-orientation="horizontal" autocomplete="off">
        <div id="calories-output">
          <h2>300 Calories</h2>
        </div>

      </div>
      <div class="col-md-4"></div>
    </div>

    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-8">
        <button type="button" id="create-meal-modal-btn" class="btn btn-primary" data-toggle="modal" data-target="#meal-idea-modal">Create Meal</button>
        <button type="button" id="reset-radio-btn" class="btn btn-primary">Reset</button>
        <table id="meal-ideas-table" class="table table-condensed">
            <tbody id="table-headings">
              <tr>
                <th>Select Your Protein Source</th>
                <th>Select Your Carb Source</th>
                <th>Select Your Fat Source</th>
              </tr>
            </tbody>

            <tbody>
              
              <tr>
                  <td>
                    @foreach($protein_foods as $food)
                      @if($food->id == 50)
                        <div class="food-name-radio-btn-container ellipsis"><input type="radio" name="food-name-protein" class="food-name-radio-btn food-name-protein" value="{{ $food->id }}" autocomplete="off" checked>{{ $food->food_name }}</div>
                      @else
                        <div class="food-name-radio-btn-container ellipsis"><input type="radio" name="food-name-protein" class="food-name-radio-btn food-name-protein" value="{{ $food->id }}" autocomplete="off">{{ $food->food_name }}</div>
                      @endif
                    @endforeach
                  </td>
                  <td>
                    @foreach($carb_foods as $food)
                      <div class="food-name-radio-btn-container ellipsis"><input type="radio" name="food-name-carbs" class="food-name-radio-btn food-name-carbs" value="{{ $food->id }}" autocomplete="off">{{ $food->food_name }}</div>
                    @endforeach
                  </td>
                  <td>
                    @foreach($fat_foods as $food)
                      <div class="food-name-radio-btn-container ellipsis"><input type="radio" name="food-name-fat" class="food-name-radio-btn food-name-fat" value="{{ $food->id }}" autocomplete="off">{{ $food->food_name }}</div>
                    @endforeach
                  </td>

              </tr>
              
            </tbody>
        </table>

        <div class="modal fade" id="meal-idea-modal" tabindex="-1" role="dialog" aria-labelledby="meal-idea-modal-label">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Meal Idea</h4>
              </div>
              <div class="modal-body">
                <table id="meal-idea-table" class="table table-condensed">
                  <tbody id="table-headings">
                    <tr>
                      <th>Food</th>
                      <th>Per</th>
                      <th>Unit</th>
                      <th>Protein</th>
                      <th>Carbs</th>
                      <th>Fat</th>
                      <th>Calories</th>
                    </tr>
                  </tbody>
                  <tbody>
                    <tr id="row-protein" class="row-macro">
                      <td class="food-name"></td>
                      <td class="per"></td>
                      <td class="unit"></td>
                      <td class="protein"></td>
                      <td class="carbs"></td>
                      <td class="fat"></td>
                      <td class="calories"></td>
                    </tr>
                    <tr id="row-carbs" class="row-macro">
                      <td class="food-name"></td>
                      <td class="per"></td>
                      <td class="unit"></td>
                      <td class="protein"></td>
                      <td class="carbs"></td>
                      <td class="fat"></td>
                      <td class="calories"></td>
                    </tr>
                    <tr id="row-fat" class="row-macro">
                      <td class="food-name"></td>
                      <td class="per"></td>
                      <td class="unit"></td>
                      <td class="protein"></td>
                      <td class="carbs"></td>
                      <td class="fat"></td>
                      <td class="calories"></td>
                    </tr>
                    <tr id="total-macros-tr">
                      <th>Total</th>
                      <th></th>
                      <td></td>
                      <th id="total-protein"></th>
                      <th id="total-carbs"></th>
                      <th id="total-fat"></th>
                      <th id="total-calories"></th>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
              </div>
            </div>
          </div>
        </div>


      </div>
      <div class="col-md-2"></div>
    </div>
    </div>
  </div>

  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="/js/core.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/meal_ideas.js"></script>

</body>

</html>