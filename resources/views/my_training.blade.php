<!DOCTYPE html>
<html lang="en">

@include('head')

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>The Macro Coach - My Training</title>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body>

  @include('navigation')

  <div class="container">
    <div class="row">
      <div class="col-sm-12">

      <select id="my-training-days-select" autocomplete="off">
          <option id="-" selected>-</option>
      </select>

      <table id="my-training-table" class="table table-condensed">
          <tbody id="table-headings">
            <tr>
              <th>Exercise</th>
              <th>Sets</th>
              <th>Rep Range</th>
              <th>Rest Period</th>
              <th>Notes</th>
              <th>Remove</th>
            </tr>
          </tbody>
          <tbody>
            <tr class="add-exercise-tr">
              <td>
                  <strong id="add-exercise" class="cursor orange">Add Exercise</strong>
              </td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>

        <button type="button" id="save-session-modal-btn" class="btn btn-primary" data-toggle="modal" data-target="#save-session-modal" disabled="disabled" autocomplete="off">
          Save Session
        </button>

        <button type="button" id="delete-session-modal-btn" class="btn btn-primary" data-toggle="modal" data-target="#delete-session-modal" disabled="disabled" autocomplete="off">
          Delete Sessions
        </button>

        <button type="button" id="duplicate-session-modal-btn" class="btn btn-primary" data-toggle="modal" data-target="#duplicate-session-modal" disabled="disabled" autocomplete="off">
          Duplicate Session
        </button>

        <div class="modal fade" id="save-session-modal" tabindex="-1" role="dialog" aria-labelledby="save-session-modal-label">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Save Sessions</h4>
              </div>
              <div class="modal-body">
                <label for="recipient-name" class="control-label">Session Name:</label>
                <input type="text" class="form-control" id="day-name" autocomplete="off" placeholder="">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="save-day-btn" class="btn btn-primary">Save</button>
              </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="delete-day-modal" tabindex="-1" role="dialog" aria-labelledby="delete-day-modal-label">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Delete Day</h4>
              </div>
              <div class="modal-body">
                Would you like to delete this day?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="delete-day-btn" class="btn btn-primary">Yes</button>
              </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="duplicate-day-modal" tabindex="-1" role="dialog" aria-labelledby="duplicate-day-modal-label">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Save Day</h4>
              </div>
              <div class="modal-body">
                Would you like to create a duplicate copy of this day?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="duplicate-day-btn" class="btn btn-primary">Yes</button>
              </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="/js/core.js"></script>
  <script src="/js/my_training.js"></script>
  <script src="/js/bootstrap.min.js"></script>
</body>

</html>