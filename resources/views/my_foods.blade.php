<!DOCTYPE html>
<html lang="en">

@include('head')

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>The Macro Coach - My Foods</title>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body>

  @include('navigation')

  <div class="container">
    <div class="row">
      <div class="col-sm-12">

      <input type="text" id="search-foods" class="form-control" placeholder="Search..." autocomplete="off">

      <table id="my-foods-table" class="table table-condensed table-hover">
          <tbody id="table-headings">
            <tr>
              <th>Food Name</th>
              <th>Per</th>
              <th>Unit</th>
              <th>Protein</th>
              <th>Carbs</th>
              <th>Fat</th>
              <th>Calories</th>
              <th>Add/Edit</th>
            </tr>

            <tr id="add-food-row">
              <td><input type="text" id="add-food-name" placeholder="Add a new food" autocomplete="off"></td>
              <td>
                <select id="add-food-per" autocomplete="off">
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                  <option value="15">15</option>
                  <option value="20">20</option>
                  <option value="25">25</option>
                  <option value="30">30</option>
                  <option value="50">50</option>
                  <option value="75">75</option>
                  <option value="100">100</option>
                </select>
              </td>
              <td>
                <select id="add-food-unit" autocomplete="off">
                  <option value="g">g</option>
                  <option value="oz">oz</option>
                  <option value="ml">ml</option>
                  <option value="fl oz">fl oz</option>
                  <option value="Cup">Cup</option>
                  <option value="Small">Small</option>
                  <option value="Medium">Medium</option>
                  <option value="Large">Large</option>
                  <option value="Serving">Serving</option>
                  <option value="Slice">Slice</option>
                  <option value="Teaspoon">Teaspoon</option>
                  <option value="Tablespoon">Tablespoon</option>
                </select>
              </td>
              <td><input type="text" id="add-food-protein" placeholder="0" autocomplete="off"></td>
              <td><input type="text" id="add-food-carbs" placeholder="0" autocomplete="off"></td>
              <td><input type="text" id="add-food-fat" placeholder="0" autocomplete="off"></td>
              <td id="add-food-calories">0</td>
              <td><span id="add-food" class="glyphicon glyphicon-plus cursor orange"></span></td>
            </tr>

            @foreach($my_foods as $food)
            <tr class="food">
              <td class="food-name" data-food-id="{{ $food->id }}">{{ $food->food_name }}</td>
              <td class="food-per"><input type="text" class="per" value="{{ $food->per }}" data-old-per="{{ $food->per }}" autocomplete="off"></td>
              <td class="food-unit">{{ $food->unit }}</td>
              <td class="food-protein" data-old-protein="{{ $food->protein }}">{{ $food->protein }}</td>
              <td class="food-carbs" data-old-carbs="{{ $food->carbs }}">{{ $food->carbs }}</td>
              <td class="food-fat" data-old-fat="{{ $food->fat }}">{{ $food->fat }}</td>
              <td class="food-calories" data-old-calories="{{ $food->calories }}">{{ $food->calories }}</td>
              <td><span data-toggle="modal" data-target="#edit-food-modal" class="glyphicon glyphicon-pencil cursor edit-food-btn orange"></span></td>
            </tr>
            @endforeach
          </tbody>
        </table>

        <div class="modal fade" id="edit-food-modal" tabindex="-1" role="dialog" aria-labelledby="edit-food-modal-label">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Food</h4>
              </div>
              <div class="modal-body">
                <table id="edit-food-table" class="table table-condensed table-striped">
                  <tr>
                    <th>Food Name</th>
                    <td><input type="text" id="edit-food-name"></td>
                  </tr>
                  <tr>
                    <th>Per</th>
                    <td>
                      <select id="edit-food-per" autocomplete="off">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                        <option value="25">25</option>
                        <option value="30">30</option>
                        <option value="50">50</option>
                        <option value="75">75</option>
                        <option value="100">100</option>
                      </select>
                    </td>
                  </tr>
                  <tr>
                    <th>Unit</th>
                    <td>
                      <select id="edit-food-unit" autocomplete="off">
                        <option value="g">g</option>
                        <option value="oz">oz</option>
                        <option value="ml">ml oz</option>
                        <option value="fl oz">fl oz</option>
                        <option value="Cup">Cup</option>
                        <option value="Small">Small</option>
                        <option value="Medium">Medium</option>
                        <option value="Large">Large</option>
                        <option value="Serving">Serving</option>
                        <option value="Slice">Slice</option>
                        <option value="Teaspoon">Teaspoon</option>
                        <option value="Tablespoon">Tablespoon</option>
                      </select>
                    </td>
                  </tr>
                  <tr>
                    <th>Protein</th>
                    <td><input type="text" id="edit-food-protein"></td>
                  </tr>
                  <tr>
                    <th>Carbs</th>
                    <td><input type="text" id="edit-food-carbs"></td>
                  </tr>
                  <tr>
                    <th>Fat</th>
                    <td><input type="text" id="edit-food-fat"></td>
                  </tr>
                  <tr>
                    <th>Calories</th>
                    <td id="edit-food-calories"></td>
                  </tr>
                    <input type="hidden" id="edit-food-id" val="">
                </table>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="delete-food-btn" class="btn btn-primary">Delete</button>
                <button type="button" id="update-food-btn" class="btn btn-primary">Update</button>
              </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="/js/core.js"></script>
  <script src="/js/my_foods.js"></script>
  <script src="/js/bootstrap.min.js"></script>
</body>

</html>