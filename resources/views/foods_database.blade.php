<!DOCTYPE html>
<html lang="en">

@include('head')

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>The Macro Coach - Foods Database</title>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body>

  @include('navigation')

  <div class="container">
    <div class="row">
      <div class="col-sm-12">

      <input type="text" id="search-foods" class="form-control" placeholder="Search..." autocomplete="off">

      <table id="my-foods-table" class="table table-hover">
          <tbody id="table-headings">
            <tr>
              <th>Food Name</th>
              <th>Per</th>
              <th>Unit</th>
              <th>Protein</th>
              <th>Carbs</th>
              <th>Fat</th>
              <th>Calories</th>
              <th>Add To Your List</th>
            </tr>

            @foreach($foods as $food)
              @if((in_array("$food->id", $food_ids)))
                <tr class="food food-added">
                  <td class="food-name" data-food-id="{{ $food->id }}">{{ $food->food_name }}</td>
                  <td class="food-per">{{ $food->per }}</td>
                  <td class="food-unit">{{ $food->unit }}</td>
                  <td class="food-protein">{{ $food->protein }}</td>
                  <td class="food-carbs">{{ $food->carbs }}</td>
                  <td class="food-fat">{{ $food->fat }}</td>
                  <td class="food-calories">{{ $food->calories }}</td>
                  <td><span class="glyphicon glyphicon-ok green success"></span></td>
                </tr>
              @else
                <tr class="food">
                  <td class="food-name" data-food-id="{{ $food->id }}">{{ $food->food_name }}</td>
                  <td class="food-per">{{ $food->per }}</td>
                  <td class="food-unit">{{ $food->unit }}</td>
                  <td class="food-protein">{{ $food->protein }}</td>
                  <td class="food-carbs">{{ $food->carbs }}</td>
                  <td class="food-fat">{{ $food->fat }}</td>
                  <td class="food-calories">{{ $food->calories }}</td>
                  <td><span class="glyphicon glyphicon-plus cursor orange add-food-db"></span></td>
                </tr>
              @endif
            @endforeach
          </tbody>
        </table>

      </div>
    </div>
  </div>

  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="/js/core.js"></script>
  <script src="/js/my_foods.js"></script>
  <script src="/js/foods_database.js"></script>
  <script src="/js/bootstrap.min.js"></script>
</body>

</html>