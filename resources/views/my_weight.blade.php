<!DOCTYPE html>
<html lang="en">

@include('head')

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

  <script src="/js/bootstrap-datepicker.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.bundle.js"></script>

  <title>The Macro Coach - My Weight</title>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>

  @include('navigation')

  <div class="container">
    <div class="row">
      <div class="col-md-12">

        <div id="my-weight-chart-container">
          <canvas id="my-weight-chart"></canvas>
        </div>

        <table id='weight-goal-table' class='table table-condensed table-striped'>
          <tbody id="table-headings">
            <tr>
              <th>Weight Goal</th>
              <th>KG</th>
              <th>LBS</th>
              <th>BMI - Body Mass Index</th>
              <th>BMR - Basal Metabolic Rate</th>
              <th>Set Goal</th>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td>What is your weight goal?</td>
              <td>
                @if(count($weight_goal))
                  <input type="text" id="weight-goal-kg" placeholder="0" value="{{ $weight_goal[0]->kg }}" autocomplete="off" maxlength="4"></td>
                @else
                  <input type="text" id="weight-goal-kg" placeholder="0" autocomplete="off" maxlength="4"></td>
                @endif
              <td>
                @if(count($weight_goal))
                  <input type="text" id="weight-goal-lbs" placeholder="0" value="{{ $weight_goal[0]->lbs }}" autocomplete="off" maxlength="5"></td>
                @else
                  <input type="text" id="weight-goal-lbs" placeholder="0" autocomplete="off" maxlength="5"></td>
                @endif
              <td id="weight-goal-bmi">
                @if(count($weight_goal))
                  {{ $weight_goal[0]->bmi }}
                @else
                  0
                @endif
              </td>
              <td id="weight-goal-bmr">
                @if(count($weight_goal))
                  {{ $weight_goal[0]->bmr }}
                @else
                  0
                @endif
              </td>
              <td><span id="set-weight-goal" class="glyphicon glyphicon-ok cursor" data-toggle="modal" data-target="#set-weight-goal-modal"></span></td>
            </tr>
            <tr>
              <td>How far you are from your goal.</td>
              <td>
                @if(count($weight_goal))
                  @if($weight_goal[0]->kg > $current_weight[0]->kg)
                    +{{ round(($weight_goal[0]->kg - $current_weight[0]->kg), 1) }}
                  @else
                    -{{ round(($current_weight[0]->kg - $weight_goal[0]->kg), 1) }}
                  @endif
                @else
                  0
                @endif
              </td>
              <td>
                @if(count($weight_goal))
                  @if($weight_goal[0]->lbs > $current_weight[0]->lbs)
                    +{{ round(($weight_goal[0]->lbs - $current_weight[0]->lbs), 1) }}
                  @else
                    -{{ round(($current_weight[0]->lbs - $weight_goal[0]->lbs), 1) }}
                  @endif
                @else
                  0
                @endif
              </td>
              <td>
                @if(count($weight_goal))
                  @if($weight_goal[0]->bmi > $current_weight[0]->bmi)
                    {{ round(($weight_goal[0]->bmi - $current_weight[0]->bmi), 1) }}
                  @else
                    +{{ round(($current_weight[0]->bmi - $weight_goal[0]->bmi), 1) }}
                  @endif
                @else
                  0
                @endif
              </td>
              <td>
                @if(count($weight_goal))
                  @if($weight_goal[0]->bmr > $current_weight[0]->bmr)
                    {{ round(($weight_goal[0]->bmr - $current_weight[0]->bmr), 1) }}
                  @else
                    +{{ round(($current_weight[0]->bmr - $weight_goal[0]->bmr), 1) }}
                  @endif
                @else
                  0
                @endif
              </td>
              <td></td>
            <tr>
          </tbody>
        </table>

        <table id='my-weight-table' class='table table-condensed table-striped'>
          <tbody id="table-headings">
            <tr>
              <th>Date</th>
              <th>KG</th>
              <th>LBS</th>
              <th>BMI - Body Mass Index</th>
              <th>BMR - Basal Metabolic Rate</th>
              <th>Add/Edit</th>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td>
                <input type="text" data-provide="datepicker" class="datepicker" id="add-weight-date" placeholder="Enter a date" autocomplete="off">
              </td>
              <td>
                <input type="text" id="add-weight-kg" placeholder="0" maxlength="5" autocomplete="off">
              </td>
              <td>
                <input type="text" id="add-weight-lbs" placeholder="0" maxlength="6" autocomplete="off">
              </td>
              <td id="add-weight-bmi">0</td>
              <td id="add-weight-bmr">0</td>
              <td><span id="add-weight" class="glyphicon glyphicon-plus cursor" data-toggle="modal" data-target="#add-weight-modal"></span></td>
            </tr>
            @foreach($weight as $row)
              <tr>
                <td class="weight-date" data-weight-id="{{ $row->id }}">{{ date("l jS F Y", strtotime($row->date)) }}</td>
                <td class="weight-kg">{{ $row->kg }}</td>
                <td class="weight-lbs">{{ $row->lbs }}</td>
                <td class="weight-bmi">{{ $row->bmi }}</td>
                <td class="weight-bmr">{{ $row->bmr }}</td>
                <td><span class="glyphicon glyphicon-pencil cursor edit-weight" data-toggle="modal" data-target="#edit-weight-modal"></span></td>
              </tr>
            @endforeach
          </tbody>
        </table>

        <div class="modal fade" id="set-weight-goal-modal" tabindex="-1" role="dialog" aria-labelledby="set-weight-goal-modal-label">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Set Weight Goal</h4>
              </div>
              <div class="modal-body">
                Would you like to set a weight goal?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="set-weight-goal-btn" class="btn btn-primary">Yes</button>
              </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="add-weight-modal" tabindex="-1" role="dialog" aria-labelledby="add-weight-modal-label">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Weight</h4>
              </div>
              <div class="modal-body">
                Would you like to add a weight?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="add-weight-btn" class="btn btn-primary">Yes</button>
              </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="edit-weight-modal" tabindex="-1" role="dialog" aria-labelledby="edit-weight-modal-label">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Weight</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" id="weight_id">
                <table id="edit-weight-table" class="table table-condensed table-striped">
                  <tr>
                    <th>Date</th>
                    <td><input type="text" data-provide="datepicker" class="datepicker" id="edit-weight-date" autocomplete="off"></td>
                  </tr>
                  <tr>
                    <th>KG</th>
                    <td><input type="text" id="edit-weight-kg"></td>
                  </tr>
                  <tr>
                    <th>LBS</th>
                    <td><input type="text" id="edit-weight-lbs"></td>
                  </tr>
                  <tr>
                    <th>BMI</th>
                    <td id="edit-weight-bmi"></td>
                  </tr>
                  <tr>
                    <th>BMR</th>
                    <td id="edit-weight-bmr"></td>
                  </tr>
                </table>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="delete-weight-btn" class="btn btn-primary">Delete</button>
                <button type="button" id="update-weight-btn" class="btn btn-primary">Update</button>
              </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="/js/core.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/my_weight.js"></script>

</body>

</html>