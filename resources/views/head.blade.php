<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Main -->
<link href="css/main.css" rel="stylesheet">
<link href="css/rangeslider.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://rawgit.com/SamWM/jQuery-Plugins/master/numeric/jquery.numeric.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/rangeslider.js/2.0.5/rangeslider.min.js"></script>
<script src="http://momentjs.com/downloads/moment.js"></script>

<!-- Favicon -->
<link rel="icon" href="http://www.themacrocoachapp.com/img/favicon.ico" type="image/x-icon"/>
