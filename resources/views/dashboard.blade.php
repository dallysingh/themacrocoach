<!DOCTYPE html>
<html lang="en">

@include('head')

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>The Macro Coach - Dashboard</title>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body>

  @include('navigation')

  <div class="container">
    <div class="row">

      <div class="col-md-4"></div>
      <div class="col-md-4">
        <h4 id="bmr-tagline">BMR (Basal Metabolic Rate)</h4>
        <input type="range" id="bmr-range-slider" min="1" max="6" step="1" value="{{ $activity_statistics[0]->activity_level }}" data-orientation="horizontal" autocomplete="off">
        <div id="bmr-output">
          <h2><span id="activity-title">{{ $activity_statistics[0]->activity_title }}</span> <span id="activity-desc">{{ $activity_statistics[0]->activity_description }}</span> <span id="bmr">{{ $activity_statistics[0]->maintenance_calories }}</span> Calories</h2>
        </div>
      </div>
      <div class="col-md-4"></div>
    </div>

    <div id="calorie-intake-row" class="row">
      <div class="col-md-4"></div>
      <div id="calorie-intake-container" class="col-md-4">
        <h4 id="bmr-tagline">Set your calorie intake</h4>
        <input type="range" id="calorie-range-slider" min="1000" max="6000" step="50" value="{{ $activity_statistics[0]->calorie_intake }}" data-orientation="horizontal" autocomplete="off">
        <div id="calories-output">
          <h2>{{ $activity_statistics[0]->calorie_intake }} Calories</h2>
        </div>
      </div>
      <div class="col-md-4"></div>
    </div>

    <div id="calorie-calculation-row" class="row">
      <div class="col-md-4"></div>
      <div id="calorie-calculation-container" class="col-md-4">
        <h4 id="bmr-tagline">Result</h4>
        <div id="calorie-calculation-output">
          <h2>
            @if(($activity_statistics[0]->calorie_intake - $activity_statistics[0]->maintenance_calories) < 0)
              <span id="calorie-intake">{{ $activity_statistics[0]->calorie_intake - $activity_statistics[0]->maintenance_calories }}</span> Calorie Deficit
            @else
              <span id="calorie-intake">{{ $activity_statistics[0]->calorie_intake - $activity_statistics[0]->maintenance_calories }}</span> Calorie Surplus
            @endif
          </h2>
        </div>
      </div>
      <div class="col-md-4"></div>
    </div>

    
  </div>

  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="/js/core.js"></script>
  <script src="/js/dashboard.js"></script>
  <script src="/js/bootstrap.min.js"></script>
</body>

</html>