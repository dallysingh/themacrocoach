<!DOCTYPE html>
<html lang="en">

@include('head')

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>The Macro Coach - Impersonate</title>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body id="login-page">

  <a href="/"><div id="logo"></div></a>

  <div id="login-errors">
    @if(count($errors))
      @foreach($errors->all() as $error)
        {{ $error }}
      @endforeach
    @endif
  </div>

  </div>

  <div class="container">
    <div class="row">
      <div class="col-sm-12">

          <div id="login-container">
            <form id="login-form" method="POST" action="{{ url('/impersonate') }}">
            {{ csrf_field() }}
              <input type="email" class="form-control" name="email" id="email" placeholder="Email address">
              <input type="password" class="form-control" name="password" id="password" placeholder="Password">
              <button type="submit" class="btn btn-default btn-block btn-primary" id="login-btn">Login</button>  
            </form>
          </div>

      </div>
    </div>
  </div>

  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="/js/core.js"></script>
  <script src="js/bootstrap.min.js"></script>
</body>

</html>