<!DOCTYPE html>
<html lang="en">

@include('head')

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>The Macro Coach - My Days</title>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body>

  @include('navigation')

  <div class="container">
    <div class="row">
      <div class="col-sm-12">

      <select id="my-days-select" autocomplete="off">
          <option id="-" selected>Create New Day</option>
        @foreach($my_days as $day)
          <option id="{{ $day->id }}">{{ $day->day_name }}</option>
        @endforeach
      </select>

      <table id="my-days-table" class="table table-condensed">
          <tbody id="table-headings">
            <tr>
              <th>Meal No.</th>
              <th>Food</th>
              <th>Per</th>
              <th>Unit</th>
              <th>Protein</th>
              <th>Carbs</th>
              <th>Fat</th>
              <th>Calories</th>
              <th>Remove</th>
            </tr>
          </tbody>
          <tbody>
            <tr class="add-meal-tr">
              <td>
                  <strong id="add-meal" class="cursor orange">Add Meal</strong>
              </td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
          <tbody>
            <tr id="maintenance-calories-tr">
              <td></td>
              <th>Maintenance Calories</th>
              <td></td>
              <td></td>
              <th></th>
              <th></th>
              <th></th>
              <th id="maintenance-calories">{{ $activity[0]->maintenance_calories }}</th>
              <td></td>
            </tr>
          </tbody>
          <tbody>
            <tr id="calorie-goal-tr">
              <td></td>
              <th>Calorie Goal</th>
              <td></td>
              <td></td>
              <th></th>
              <th></th>
              <th></th>
              <th id="calorie-goal">{{ $activity[0]->calorie_intake }}</th>
              <td></td>
            </tr>
          </tbody>
          <tbody>
            <tr id="macro-ratio-tr">
              <td></td>
              <th>Macro Ratio</th>
              <td></td>
              <td></td>
              <th id="macro-ratio-protein"></th>
              <th id="macro-ratio-carbs"></th>
              <th id="macro-ratio-fat"></th>
              <td></td>
              <td></td>
            </tr>
          </tbody>
          <tbody>
            <tr id="total-macros-tr">
              <td></td>
              <th>Total</th>
              <td></td>
              <td></td>
              <th id="total-protein"></th>
              <th id="total-carbs"></th>
              <th id="total-fat"></th>
              <th id="total-calories"></th>
              <td></td>
            </tr>
          </tbody>
        </table>

        <button type="button" id="save-day-modal-btn" class="btn btn-primary" data-toggle="modal" data-target="#save-day-modal" disabled="disabled" autocomplete="off">
          Save Day
        </button>

        <button type="button" id="delete-day-modal-btn" class="btn btn-primary" data-toggle="modal" data-target="#delete-day-modal" disabled="disabled" autocomplete="off">
          Delete Day
        </button>

        <button type="button" id="duplicate-day-modal-btn" class="btn btn-primary" data-toggle="modal" data-target="#duplicate-day-modal" disabled="disabled" autocomplete="off">
          Duplicate Day
        </button>

        <div class="modal fade" id="save-day-modal" tabindex="-1" role="dialog" aria-labelledby="save-day-modal-label">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Save Day</h4>
              </div>
              <div class="modal-body">
                <label for="recipient-name" class="control-label">Day Name:</label>
                <input type="text" class="form-control" id="day-name" autocomplete="off" placeholder="">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="save-day-btn" class="btn btn-primary">Save</button>
              </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="delete-day-modal" tabindex="-1" role="dialog" aria-labelledby="delete-day-modal-label">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Delete Day</h4>
              </div>
              <div class="modal-body">
                Would you like to delete this day?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="delete-day-btn" class="btn btn-primary">Yes</button>
              </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="duplicate-day-modal" tabindex="-1" role="dialog" aria-labelledby="duplicate-day-modal-label">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Save Day</h4>
              </div>
              <div class="modal-body">
                Would you like to create a duplicate copy of this day?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="duplicate-day-btn" class="btn btn-primary">Yes</button>
              </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="/js/core.js"></script>
  <script src="/js/my_days.js"></script>
  <script src="/js/bootstrap.min.js"></script>
</body>

</html>