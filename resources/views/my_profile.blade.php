<!DOCTYPE html>
<html lang="en">

@include('head')

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

  <script src="/js/bootstrap-datepicker.js"></script>

  <title>The Macro Coach - My Profile</title>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body>

  @include('navigation')

  <div class="container">
    <div class="row">
      <div class="col-sm-12">

        <!-- Nav tabs -->
        <ul id="my-profile-tabs" class="nav nav-tabs" role="tablist">
          <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
          <li role="presentation"><a href="#password" aria-controls="password" role="tab" data-toggle="tab">Password</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane fade in active" id="profile">
            <form action="update-profile" method="post" id="update-profile">
              <input type="text" class="" name="first-name" id="first-name" placeholder="{{ $user[0]->first_name }}" autocomplete="off">
              <input type="text" class="" name="surname" id="surname" placeholder="{{ $user[0]->surname }}" autocomplete="off">
              <input type="email" class="" name="email" id="email" placeholder="{{ $user[0]->email }}" autocomplete="off" readonly>
              <input type="text" data-provide="datepicker" class="datepicker" id="dob" placeholder="{{ $dob }}" autocomplete="off">
              <select id="gender" class="">
                @if($profile[0]->gender == 'male')
                <option value="male" selected>Male</option>
                <option value="female">Female</option>
                @else
                <option value="male">Male</option>
                <option value="female" selected>Female</option>
                @endif
              </select>
              <div id="height-container" class="clearfix">
                <input type="text" class="" name="height-cm" id="height-cm" placeholder="{{ $profile[0]->height_cm }} cm" autocomplete="off">
                <input type="text" class="" name="height-ft" id="height-ft" placeholder="{{ $profile[0]->height_ft }} ft" autocomplete="off">
                <input type="text" class="" name="height-inch" id="height-inch" placeholder='{{ $profile[0]->height_inch }} "' autocomplete="off">
              </div>
              <button type="submit" class="btn btn-default btn-block btn-primary update-btn" id="update-profile-btn">Update</button>
            </form>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="password">
            <form action="update-password" method="post">
              <input type="password" class="" name="old-password" id="old-password" placeholder="Old Password">
              <input type="password" class="" name="password" id="password" placeholder="Password">
              <input type="password" class="" name="confirm-password" id="confirm-password" placeholder="Confirm Password">

              <button type="submit" class="btn btn-default btn-block btn-primary update-btn" id="update-password-btn">Update</button>
            </form>
          </div>
        </div>

      </div>
    </div>
  </div>

  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="/js/core.js"></script>
  <script src="/js/my_profile.js"></script>
  <script src="/js/bootstrap.min.js"></script>
</body>

</html>