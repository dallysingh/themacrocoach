$( document ).ready(function() {

	$("#add-food-protein").numeric();
	$("#edit-food-protein").numeric();
	$("#add-food-carbs").numeric();
	$("#edit-food-carbs").numeric();
	$("#add-food-fat").numeric();
	$("#edit-food-fat").numeric();
	$(".per").numeric();

	var protein = 0;
	var carbs = 0;
	var fat = 0;
	var calories = 0

	$("body").on('keyup', '#add-food-protein', function()
    {
    	protein = $(this).val();
    	calories = calculateFoodCalories(protein, carbs, fat);
    	$("#add-food-calories").html(calories);
    });

    $("body").on('keyup', '#add-food-carbs', function()
    {
    	carbs = $(this).val();
    	calories = calculateFoodCalories(protein, carbs, fat);
    	$("#add-food-calories").html(calories);
    });

    $("body").on('keyup', '#add-food-fat', function()
    {
    	fat = $(this).val();
    	calories = calculateFoodCalories(protein, carbs, fat);
    	$("#add-food-calories").html(calories);
    });

    $("body").on('click', '#add-food', function()
    {
    	var food_name = $("#add-food-name").val();

    	if(food_name.length == 0)
    	{
    		$("#add-food-name").addClass("empty-field");
    	}
    	else
    	{
    		var per = $("#add-food-name").closest('tr').find('#add-food-per option:selected').val();
    		var unit = $("#add-food-name").closest('tr').find('#add-food-unit option:selected').val();
    		protein = $("#add-food-name").closest('tr').find('#add-food-protein').val();
    		carbs = $("#add-food-name").closest('tr').find('#add-food-carbs').val();
    		fat = $("#add-food-name").closest('tr').find('#add-food-fat').val();
    		calories = $("#add-food-name").closest('tr').find('#add-food-calories').html();

    		if(protein.length == 0) { protein = 0; }
    		if(carbs.length == 0) { carbs = 0; }
    		if(fat.length == 0) { fat = 0; }

    		$("#add-food-name").removeClass("empty-field");

    		$.ajax({
				type : "POST",
				url : "add-food",
				data: { "food_name" : food_name, "per" : per, "unit" : unit, "protein" : protein, "carbs" : carbs, "fat" : fat, "calories" : calories },
				success : function(data)
				{
				    location.reload();
				}
			})
    	}
    });

    $("body").on("keyup", ".per", function()
    {
    	var obj = this;
    	calculateFoodMacros(obj);
    });

    $('body').on("click", '.edit-food-btn', function()
    {
    	var food_id = $(this).closest('tr').find('.food-name').data('food-id')
    	var food_name = $(this).closest('tr').find('.food-name').html();
    	var per = $(this).closest('tr').find('.per').data('old-per');
    	var unit = $(this).closest('tr').find('.food-unit').html();
    	protein = $(this).closest('tr').find('.food-protein').data('old-protein');
    	carbs = $(this).closest('tr').find('.food-carbs').data('old-carbs');
    	fat = $(this).closest('tr').find('.food-fat').data('old-fat');
    	calories = $(this).closest('tr').find('.food-calories').html();

    	$("#edit-food-id").val(food_id);
    	$("#edit-food-name").val(food_name);
    	$("#edit-food-per").val(per);
    	$("#edit-food-unit").val(unit);
    	$("#edit-food-protein").val(protein);
    	$("#edit-food-carbs").val(carbs);
    	$("#edit-food-fat").val(fat);
    	$("#edit-food-calories").html(calories);
    });

    $("body").on('keyup', '#edit-food-protein', function()
    {
    	protein = $(this).val();
    	calories = calculateFoodCalories(protein, carbs, fat);
    	$("#edit-food-calories").html(calories);
    });

    $("body").on('keyup', '#edit-food-carbs', function()
    {
    	carbs = $(this).val();
    	calories = calculateFoodCalories(protein, carbs, fat);
    	$("#edit-food-calories").html(calories);
    });

    $("body").on('keyup', '#edit-food-fat', function()
    {
    	fat = $(this).val();
    	calories = calculateFoodCalories(protein, carbs, fat);
    	$("#edit-food-calories").html(calories);
    });

    $('body').on('click', "#update-food-btn", function()
    {
    	var food_id = $("#edit-food-id").val();
    	var food_name = $("#edit-food-name").val();
    	var per = $("#edit-food-per option:selected").val();
    	var unit = $("#edit-food-unit option:selected").val();
    	protien = $("#edit-food-protein").val();
    	carbs = $("#edit-food-carbs").val();
    	fat  = $("#edit-food-fat").val();
    	calories = $("#edit-food-calories").html();

    	if(food_name.length == 0)
    	{
    		$("#edit-food-name").addClass("empty-field");
    	}
    	else
    	{
    		$("#edit-food-name").removeClass("empty-field");

	    	$.ajax({
				type : "POST",
				url : "update-food",
				data: { "food_id" : food_id, "food_name" : food_name, "per" : per, "unit" : unit, "protein" : protein, "carbs" : carbs, "fat" : fat, "calories" : calories },
				success : function(data)
				{
				    location.reload();
				}
			})
		}
    });

    $('body').on('click', '#delete-food-btn', function()
    {
    	var food_id = $("#edit-food-id").val();

    	$.ajax({
			type : "POST",
			url : "delete-food",
			data: { "food_id" : food_id },
			success : function(data)
			{
			    location.reload();
			}
		})
    });

    $('body').on('keyup', '#search-foods', function()
    {
    	var search_item = $(this).val();

    	$('.food-name').each(function()
		{
			if($(this).html().toLowerCase().indexOf(search_item.toLowerCase()) != 0)
			{
				$(this).closest('tr').hide();
			}
			else
			{
				$(this).closest('tr').show();
			}
		});
    });

    function calculateFoodCalories(protein, carbs, fat)
    {
        calories = (protein * 4) + (carbs * 4) + (fat * 9);
        return calories;
    }

    function calculateFoodMacros(obj)
    {
        var per = $(obj).val();
        var old_per = $(obj).data('old-per');
        var food_protein = $(obj).closest('tr').find('.food-protein').data('old-protein');
        var food_carbs = $(obj).closest('tr').find('.food-carbs').data('old-carbs');
        var food_fat = $(obj).closest('tr').find('.food-fat').data('old-fat');

        food_protein = Math.round((food_protein / old_per) * per);
        food_carbs = Math.round((food_carbs / old_per) * per);
        food_fat = Math.round((food_fat / old_per) * per);
        food_calories = (food_protein * 4) + (food_carbs * 4) + (food_fat * 9);

        $(obj).closest('tr').find('.food-protein').html(food_protein);
        $(obj).closest('tr').find('.food-carbs').html(food_carbs);
        $(obj).closest('tr').find('.food-fat').html(food_fat);
        $(obj).closest('tr').find('.food-calories').html(food_calories);

    }
});