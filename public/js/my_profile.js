$( document ).ready(function() {

	var bmr;
	var kg;
	var activity_level;
	var maintenance_calories;
	var today = new Date();

	$.ajax({
		type : "GET",
		url : "get-my-current-weight",
		data: { },
		success : function(data)
		{
		    kg = data[0]['kg'];
		}
	})

	$.ajax({
		type : "GET",
		url : "get-activity",
		data: { },
		success : function(data)
		{
		    activity_level = data[0]['activity_level'];
		}
	})

	$("#height-cm").numeric();
	$("#height-ft").numeric();
	$("#height-inch").numeric();

	$('#my-profile-tabs a').click(function (e) {
	  e.preventDefault()
	  $(this).tab('show')
	})

	$("#my-profile-tabs li:first").addClass("active");

	$('.datepicker').datepicker({
		format: 'dd/mm/yyyy',
		endDate: '+0d',
		weekStart: 1
	});

	$("#update-profile").on('submit', function(e)
	{
		e.preventDefault();

		if($("#first-name").val().length == 0) { var first_name = $("#first-name").attr('placeholder'); } else { var first_name = $("#first-name").val(); }
		if($("#surname").val().length == 0) { var surname = $("#surname").attr('placeholder'); } else { var surname = $("#surname").val(); }
		if($("#email").val().length == 0) { var email = $("#email").attr('placeholder'); } else { var email = $("#email").val(); }
		if($("#dob").val().length == 0) { var dob = $("#dob").attr('placeholder'); } else { var dob = $("#dob").val(); }
		if($("#height-cm").val().length == 0) { var height_cm = $("#height-cm").attr('placeholder'); } else { var height_cm = $("#height-cm").val(); }
		if($("#height-ft").val().length == 0) { var height_ft = $("#height-ft").attr('placeholder'); } else { var height_ft = $("#height-ft").val(); }
		if($("#height-inch").val().length == 0) { var height_inch = $("#height-inch").attr('placeholder'); } else { var height_inch = $("#height-inch").val(); }
		height_cm = height_cm.replace(" cm", "");
		height_ft = height_ft.replace(" ft", "");
		height_inch = height_inch.replace(' "', "");

		var gender = $("#gender option:selected").val();
		calculateBMIandBMR(kg, height_cm, gender, dob);

		$.ajax({
			type : "POST",
			url : "update-profile",
			data: { "first_name" : first_name, "surname" : surname, "email" : email, "dob" : dob, "gender" : gender, "height_cm" : height_cm, "height_ft" : height_ft, "height_inch" : height_inch, "bmr" : bmr, "maintenance_calories" : maintenance_calories},
			success : function(data)
			{
			    location.reload();
			}
		})
	});

	// Convert cm to ft & inches.
	$("#height-cm").bind("keyup keydown", function()
	{
		convertCM($(this).val());

		if($(this).val().length == 0)
		{
			$("#height-ft").val("");
			$("#height-inch").val("");
		}
		else
		{
			$("#height-ft").val(ft);
			$("#height-inch").val(inch);
		}
	});

	// Convert ft to cm.
	$("#height-ft").bind("keyup keydown", function()
	{
		if($("#height-inch").length == 0)
		{
			var inch = 0;
		}
		else
		{
			var inch = $("#height-inch").val();
		}

		convertFTInches($(this).val(), inch);

		if($(this).val().length == 0 && $("#height-inch").length == 0)
		{
			$("#height-cm").val("");
		}
		else
		{
			$("#height-cm").val(cm);
		}
	});

	// Convert inches to cm.
	$("#height-inch").bind("keyup keydown", function()
	{
		if($("#height-ft").length == 0)
		{
			var ft = 0;
		}
		else
		{
			var ft = $("#height-ft").val();
		}

		convertFTInches(ft, $(this).val());

		if($(this).val().length == 0 && $("#height-ft").length == 0)
		{
			$("#height-cm").val("");
		}
		else
		{
			$("#height-cm").val(cm);
		}
	});

	// Convert cm to ft & inches.
	function convertCM(cm)
	{
		inch = cm * 0.39370;
		ft = inch * 0.083333;
		decimal = ft - Math.floor(ft);	
		inch = decimal * 12;

		ft = Math.floor(ft);
		inch = Math.floor(inch);
	}

	// Convert feet & inches to cm.
	function convertFTInches(ft, inch)
	{
		ft_to_cm = ft / 0.032808;
		inch_to_cm = inch / 0.39370;

		cm = Math.floor(ft_to_cm + inch_to_cm);
	}

	// Calculate a persons BMI & BMR.
	function calculateBMIandBMR(kg, cm, gender, dob)
	{
		var bmi = ((kg / ((cm / 100) * (cm / 100)))).toFixed(1);

		var date_array = dob.split('/');
		var new_dob = date_array[2] + '-' + date_array[1] + '-' + date_array[0].slice(-2);

		var age = today.getYear() - new Date(new_dob).getYear() - 1;
		
		if(kg > 0 && gender == "male")
		{
			// Harris-Benedict Formula
			// bmr = (66 + (13.7 * kg) + (5 * cm) - (6.8 * age)).toFixed(1);

			// Mifflin St Jeor Formula
			bmr = ((10 * kg) + (6.25 * cm) - (5 * age) + 5).toFixed(1);
		}
		else if(kg > 0 && gender == "female")
		{
			// Harris-Benedict Formula
			// bmr = (655 + (9.6 * kg) + (1.8 * cm) - (4.7 * age)).toFixed(1);

			// Mifflin St Jeor Formula
			bmr = ((10 * kg) + (6.25 * cm) - (5 * age) - 161).toFixed(1);
		}
		else
		{
			bmr = 0;
		}

		if(activity_level == 1) { var multiplier = 0 };
		if(activity_level == 2) { var multiplier = 1.2 };
		if(activity_level == 3) { var multiplier = 1.375 };
		if(activity_level == 4) { var multiplier = 1.55 };
		if(activity_level == 5) { var multiplier = 1.725 };
		if(activity_level == 6) { var multiplier = 1.9 };

		maintenance_calories = bmr * multiplier;
	}

});