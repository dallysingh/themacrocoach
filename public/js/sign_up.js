$( document ).ready(function() {

	var bmi = 0;
	var bmr = 0;
	var today = new Date();

	$("#weight-kg").numeric();
	$("#weight-lbs").numeric();
	$("#height-cm").numeric();
	$("#height-ft").numeric();
	$("#height-inch").numeric();

	$('.datepicker').datepicker({
		format: 'dd/mm/yyyy',
		endDate: '+0d',
		weekStart: 1
	});

	$("#weight-kg").bind("keyup keydown", function()
	{
		kg = $(this).val();
		lbs = convertKG(kg);

		$("#weight-lbs").val(lbs);
	});

	$("#weight-lbs").bind("keyup keydown", function()
	{
		lbs = $(this).val();
		kg = convertLBS(lbs);

		$("#weight-kg").val(kg);
	});

	// Convert cm to ft & inches.
	$("#height-cm").bind("keyup keydown", function()
	{
		convertCM($(this).val());

		if($(this).val().length == 0)
		{
			$("#height-ft").val("");
			$("#height-inch").val("");
		}
		else
		{
			$("#height-ft").val(ft);
			$("#height-inch").val(inch);
		}
	});

	// Convert ft to cm.
	$("#height-ft").bind("keyup keydown", function()
	{
		if($("#height-inch").length == 0)
		{
			var inch = 0;
		}
		else
		{
			var inch = $("#height-inch").val();
		}

		convertFTInches($(this).val(), inch);

		if($(this).val().length == 0 && $("#height-inch").length == 0)
		{
			$("#height-cm").val("");
		}
		else
		{
			$("#height-cm").val(cm);
		}
	});

	// Convert inches to cm.
	$("#height-inch").bind("keyup keydown", function()
	{
		if($("#height-ft").length == 0)
		{
			var ft = 0;
		}
		else
		{
			var ft = $("#height-ft").val();
		}

		convertFTInches(ft, $(this).val());

		if($(this).val().length == 0 && $("#height-ft").length == 0)
		{
			$("#height-cm").val("");
		}
		else
		{
			$("#height-cm").val(cm);
		}
	});

	$("#sign-up-form").on('submit', function(e) {
        e.preventDefault();

        var empty_fields = 0;
        var incorrect_fields = 0;

        $(".check-field").removeClass("empty-field");
        $(".check-field").each(function(e)
        {
        	if($(this).val().length == 0)
        	{
        		empty_fields++;
        		$(this).addClass("empty-field");
        	}
        });

       	if(empty_fields == 0)
       	{
       		$(".check-field").removeClass("empty-field");

       		var first_name = $("#first-name").val();
       		var surname = $("#surname").val();
       		var email = $("#email").val();
       		var password = $("#password").val();
       		var confirm_password = $("#confirm-password").val();
       		var dob = $("#dob").val();
       		var gender = $("#gender option:selected").val();
       		var weight_kg = $("#weight-kg").val();
       		var weight_lbs = $("#weight-lbs").val();
       		var height_cm = $("#height-cm").val();
       		var height_ft = $("#height-ft").val();
       		var height_inch = $("#height-inch").val();

       		if(password != confirm_password)
       		{
       			incorrect_fields++;
       			$("#password").addClass("empty-field");
       			$("#confirm-password").addClass("empty-field");
       		}

   			if(incorrect_fields == 0)
   			{
   				calculateBMIandBMR(weight_kg, height_cm, gender, dob);
   				
   				$("#loader").show();
   				$.ajax({
					type : "POST",
					url : "sign-up",
					data: { "first_name" : first_name, "surname" : surname, "email" : email, "password" : password, "dob" : dob, "gender" : gender, "weight_kg" : weight_kg, "weight_lbs" : weight_lbs, "bmi" : bmi, "bmr" : bmr, "height_cm" : height_cm, "height_ft" : height_ft, "height_inch" : height_inch },
					success : function(data)
					{
						if(data == "fail")
						{
							$("#signup-errors").html("Email address is already registered.");
						}
						else
						{
							window.location.href = "http://www.themacrocoachapp.com";
						}
					    
					}
				})
   			}
       	}
    });

	// Calculate a persons BMI & BMR.
	function calculateBMIandBMR(kg, cm, gender, dob)
	{
		bmi = ((kg / ((cm / 100) * (cm / 100)))).toFixed(1);

		var date_array = dob.split('/');
		var new_dob = date_array[2] + '-' + date_array[1] + '-' + date_array[0].slice(-2);

		var age = today.getYear() - new Date(new_dob).getYear() - 1;
		
		if(kg > 0 && gender == "male")
		{
			// Harris-Benedict Formula
			// bmr = (66 + (13.7 * kg) + (5 * cm) - (6.8 * age)).toFixed(1);

			// Mifflin St Jeor Formula
			bmr = ((10 * kg) + (6.25 * cm) - (5 * age) + 5).toFixed(1);
		}
		else if(kg > 0 && gender == "female")
		{
			// Harris-Benedict Formula
			// bmr = (655 + (9.6 * kg) + (1.8 * cm) - (4.7 * age)).toFixed(1);

			// Mifflin St Jeor Formula
			bmr = ((10 * kg) + (6.25 * cm) - (5 * age) - 161).toFixed(1);
		}
		else
		{
			bmr = 0;
		}
	}

	// Convert kg to lbs.
	function convertKG(kg)
	{
		lbs = (kg * 2.2046).toFixed(1);
		return lbs;
	}

	// Convert lbs to kg.
	function convertLBS(lbs)
	{
		kg = (lbs / 2.2046).toFixed(1);
		return kg;
	}

	// Convert cm to ft & inches.
	function convertCM(cm)
	{
		inch = cm * 0.39370;
		ft = inch * 0.083333;
		decimal = ft - Math.floor(ft);	
		inch = decimal * 12;

		ft = Math.floor(ft);
		inch = Math.floor(inch);
	}

	// Convert feet & inches to cm.
	function convertFTInches(ft, inch)
	{
		ft_to_cm = ft / 0.032808;
		inch_to_cm = inch / 0.39370;

		cm = Math.floor(ft_to_cm + inch_to_cm);
	}

});