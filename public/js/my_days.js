$( document ).ready(function() {

	var days;
	var foods = [];
	var meal_number_global = 1;

	// Get all my foods.
	$.ajax({
		type : "GET",
		url : "get-my-foods",
		dataType: "json",
		success : function(data)
		{
		    for(var i = 0; i < data.length; i++)
		    {
		    	temp = new Object();
		    	temp.food_id = data[i]['id'];
		    	temp.food_name = data[i]['food_name'];
		    	temp.per = data[i]['per'];
		    	temp.unit = data[i]['unit'];
		    	temp.protein = data[i]['protein'];
		    	temp.carbs = data[i]['carbs'];
		    	temp.fat = data[i]['fat'];
		    	temp.calories = data[i]['calories'];

		    	foods.push(temp);
		    }
		}
	})

	// Get all my meals.
	$.ajax({
		type : "GET",
		url : "get-my-meals",
		dataType: "json",
		success : function(data)
		{
		   days = data;
		}
	})

	// Add a new meal.
	$("body").on('click', '#add-meal', function()
	{
		if($('.meal').length != 0)
		{
			meal_number_global = $('.meal').length + 1;
		}

        var row = "<tbody id='meal-"+meal_number_global+"' class='meal'><tr class='food-row'><td>"+meal_number_global+"</td><td><select class='foods select-temp'></select></td><td class='per-td'><input type='text' class='per'></td><td class='unit'></td><td class='protein'></td><td class='carbs'></td><td class='fat'></td><td class='calories'></td><td class='remove-food-td'><span class='glyphicon glyphicon-remove cursor remove-food orange' data-meal="+meal_number_global+"></span></td></tr><tr id='meal-"+meal_number_global+"-total' class='total-meal-macros-tr'><td></td><th>Total</th><td></td><td></td><th class='meal_protein'></th><th class='meal_carbs'></th><th class='meal_fat'></th><th class='meal_calories'></th><th></th></tr><tr class='add-food-tr'><<td><strong class='cursor add-food orange' data-meal="+meal_number_global+">Add Food</strong</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tbody>";
        $(this).closest('tr.add-meal-tr').closest('tbody').before(row);
        $('#save-day-modal-btn').attr("disabled", false);

        populateFoods();
        populateMealData(meal_number_global);

        meal_number_global++;
        $(".per").numeric();
    });

	// Add a new food to a meal.
    $("body").on('click', '.add-food', function()
	{
		var meal_number = $(this).data('meal');
        var row = "<tr class='food-row'><td></td><td><select class='foods select-temp'></select></td><td class='per-td'><input type='text' class='per'></td><td class='unit'></td><td class='protein'></td><td class='carbs'></td><td class='fat'></td><td class='calories'></td><td class='remove-food-td'><span class='glyphicon glyphicon-remove cursor remove-food orange' data-meal="+meal_number+"></span></td></tr>";
        $(this).parent().parent().prev('tr.total-meal-macros-tr').before(row);

        populateFoods();
        populateMealData(meal_number);
        $(".per").numeric();
    });

    // Changes the macro's of a food in the table when selecting a new food from the dropdown menu.
    $("body").on('change', '.foods', function()
    {
    	var food_id = $(this).val();
    	var food = $.grep(foods, function(e){ return e.food_id == food_id; });

    	$(this).closest('tr').find('.per-td').find('.per').val(food[0].per);
    	$(this).closest('tr').find('.unit').html(food[0].unit);
    	$(this).closest('tr').find('.protein').html(food[0].protein);
    	$(this).closest('tr').find('.carbs').html(food[0].carbs);
    	$(this).closest('tr').find('.fat').html(food[0].fat);
    	$(this).closest('tr').find('.calories').html(food[0].calories);

    	var meal_number = $(this).closest('tr').nextAll('.add-food-tr:first').find('td').find('.add-food').data('meal');
    	populateMealData(meal_number);
    });

    // Change the macro's of a food when adjuting the weight of the food.
    $("body").on('keyup', '.per', function()
    {
    	var obj = this;
    	calculateMacros(obj);
    });

    // Remove a food from a meal.
    $("body").on('click', '.remove-food', function()
    {
    	var meal_number = $(this).data('meal');
    	var number_of_foods_in_meals = $('#meal-'+meal_number+' .food-row').length;

    	if(number_of_foods_in_meals != 1)
    	{
    		$(this).closest('tr').remove();
    		$('#meal-'+meal_number+ ' .food-row:nth-child(1) td:nth-child(1)').html(meal_number);
    	}
    	else
    	{
    		$(this).closest('tbody').remove();
    		meal_number_global = $('.meal').length;

    		if(meal_number_global == 0)
		    {
		    	$('#save-day-modal-btn').attr("disabled", true);
		    	meal_number_global = 1;
		    }

		    console.log(meal_number_global);

    		$('.meal').each(function(i, obj) {
    			var id = i + 1;
			    $(this).attr('id', 'meal-'+id);
			    $(this).find('.food-row:first').find('td:first').html(id);
			    $(this).find('.add-food-tr').find('td:first').find('.add-food').attr('data-meal', id);
			    $(this).find('.food-row').find('.remove-food-td').find('.remove-food').attr('data-meal', id);
			    $(this).find('.total-meal-macros-tr').attr("id", "meal-"+id+"-total");
			});
    	}

    	populateMealData(meal_number);
    });

	// Change days.
	$("#my-days-select").on('change', function()
	{
		var day_id = $(this).find('option:selected').attr('id');
		populateDay(day_id);
		$("#day-name-error").remove();

		if(day_id == "-")
		{
			$('#save-day-modal-btn').attr("disabled", true);
			$('#delete-day-modal-btn').attr("disabled", true);
			$('#duplicate-day-modal-btn').attr("disabled", true);
		}
		else
		{
			$('#save-day-modal-btn').attr("disabled", false);
			$('#delete-day-modal-btn').attr("disabled", false);
			$('#duplicate-day-modal-btn').attr("disabled", false);
		}

		meal_number_global = $(".meal").length + 1;
		$(".per").numeric();
	});

	// Opens the save day modal.
	$("#save-day-modal-btn").click(function()
	{
		var day_name_old = $("#my-days-select option:selected").val();

		if(day_name_old != '-')
		{
			$("#day-name").attr('placeholder', day_name_old);
		}
		else
		{
			$("#day-name").attr('placeholder', '');
		}
	});

	// Save or update a day.
	$("#save-day-btn").click(function()
	{
		var day_id = $("#my-days-select option:selected").attr('id');
		var day_name = $("#day-name").val();

		if(day_name.length == 0 && $("#day-name").attr('placeholder').length == 0)
		{
			var html = '<div class="modal-body" id="day-name-error"><div class="alert alert-danger" role="alert">Day name cannot be left empty.</div></div>';
			$(".modal-footer").after(html);
		}
		else if(day_name.length == 0 && $("#day-name").attr('placeholder').length > 0)
		{
			day_name = $("#day-name").attr('placeholder');
			saveDay(day_name, day_id, 0);
		}
		else
		{
			saveDay(day_name, day_id, 0);
		}
	});

	// Delete a day.
	$("#delete-day-btn").click(function()
	{
		var day_id = $("#my-days-select option:selected").attr('id');
		deleteDay(day_id);
	});

	// Duplicate a day.
	$("#duplicate-day-btn").click(function()
	{
		var day_id = $("#my-days-select option:selected").attr('id');
		var day_name = $("#my-days-select option:selected").val()+"_copy";

		saveDay(day_name, day_id, 1);
	});

	// Populate the dropdown with foods.
	function populateFoods()
	{
		$.each(foods, function(key, value) {   
			$('.select-temp').append($("<option></option>").attr("value", foods[key].food_id).text(foods[key].food_name));
		});
		$('.select-temp').closest('tr').find('.per-td').find('.per').val(foods[0].per);
		$('.select-temp').closest('tr').find('.unit').html(foods[0].unit);
		$('.select-temp').closest('tr').find('.protein').html(foods[0].protein);
		$('.select-temp').closest('tr').find('.carbs').html(foods[0].carbs);
		$('.select-temp').closest('tr').find('.fat').html(foods[0].fat);
		$('.select-temp').closest('tr').find('.calories').html(foods[0].calories);
		$('.select-temp').removeClass('select-temp');
	}

	// Populate the macro' for all the meals and the total day macro's.
	function populateMealData(meal_number)
	{
		var meal_protein = 0;
		var meal_carbs = 0;
		var meal_fat = 0;
		var meal_calories = 0;

		var total_protein = 0;
		var total_carbs = 0;
		var total_fat = 0;
		var total_calories = 0;

		$('#meal-'+meal_number+ ' .protein').each(function(i, obj) {
	   		meal_protein = meal_protein + parseInt($(this).html());
		});

		$('#meal-'+meal_number+ ' .carbs').each(function(i, obj) {
	   		meal_carbs = meal_carbs + parseInt($(this).html());
		});

		$('#meal-'+meal_number+ ' .fat').each(function(i, obj) {
	   		meal_fat = meal_fat + parseInt($(this).html());
		});

		$('#meal-'+meal_number+ ' .calories').each(function(i, obj) {
	   		meal_calories = meal_calories + parseInt($(this).html());
		});

		$('#meal-'+meal_number+'-total .meal_protein').html(meal_protein);
		$('#meal-'+meal_number+'-total .meal_carbs').html(meal_carbs);
		$('#meal-'+meal_number+'-total .meal_fat').html(meal_fat);
		$('#meal-'+meal_number+'-total .meal_calories').html(meal_calories);

		$('.meal_protein').each(function(i, obj) {
			total_protein = total_protein + parseInt($(obj).html());
		});
		$("#total-protein").html(total_protein);

		$('.meal_carbs').each(function(i, obj) {
			total_carbs = total_carbs + parseInt($(obj).html());
		});
		$("#total-carbs").html(total_carbs);

		$('.meal_fat').each(function(i, obj) {
			total_fat = total_fat + parseInt($(obj).html());
		});
		$("#total-fat").html(total_fat);

		$('.meal_calories').each(function(i, obj) {
			total_calories = total_calories + parseInt($(obj).html());
		});
		$("#total-calories").html(total_calories);

		$("#macro-ratio-protein").html( Math.round(((total_protein * 4) / total_calories) * 100) + "%" );
		$("#macro-ratio-carbs").html( Math.round(((total_carbs * 4) / total_calories) * 100) + "%" );
		$("#macro-ratio-fat").html( Math.round(((total_fat * 9) / total_calories) * 100) + "%" );
	}

	// Calculate macro's for a food.
	function calculateMacros(obj)
	{
		var food_id = $(obj).closest('td').prev('td').find('.foods').val();
	    var food = $.grep(foods, function(e){ return e.food_id == food_id; });

	    var per_new = $(obj).val();
		var per_old = food[0].per;
		var protein_old = food[0].protein;
		var carbs_old = food[0].carbs;
		var fat_old = food[0].fat;

		var protein_new = Math.round(((protein_old/per_old) * per_new));
		var carbs_new = Math.round(((carbs_old/per_old) * per_new));
		var fat_new = Math.round(((fat_old/per_old) * per_new));
		var calories = ((protein_new * 4) + (carbs_new * 4) + (fat_new * 9));

		$(obj).closest('tr').find('.protein').html(protein_new);
		$(obj).closest('tr').find('.carbs').html(carbs_new);
		$(obj).closest('tr').find('.fat').html(fat_new);
		$(obj).closest('tr').find('.calories').html(calories);

		meal_number = $(obj).closest('tbody').attr('id');
		meal_number = meal_number.replace("meal-", "");
		
	   	populateMealData(meal_number);
	   	targetGoalCalories();
	}

	// Populate the table with the selected day from the dropdown.
	function populateDay(day_id)
	{
		$(".table .meal").remove();

		var meal_number = 0;
		var day = $.grep(days, function(e){ return e.day_id == day_id; });

		$(day).each(function(i, obj) {

			var tbody = "<tbody id='meal-"+obj.meal_number+"' class='meal'><tr id='meal-"+obj.meal_number+"-total' class='total-meal-macros-tr'><td></td><th>Total</th><td></td><td></td><th class='meal_protein'></th><th class='meal_carbs'></th><th class='meal_fat'></th><th class='meal_calories'></th><th></th></tr><tr class='add-food-tr'><<td><strong class='cursor add-food orange' data-meal="+obj.meal_number+">Add Food</strong</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tbody>"

			if(meal_number != obj.meal_number)
			{
				$(".add-meal-tr").parent().before(tbody);
			}
			meal_number = obj.meal_number;
		});

		var number_of_meals = $(".meal").length;

		for(var i = 1; i <= number_of_meals; i++)
		{
			var meal_number = 0;
			var meal = $.grep(day, function(e){ return e.meal_number == i; });

			$(meal).each(function(i, obj)
			{
				if(meal_number != obj.meal_number)
				{
					var row = "<tr class='food-row'><td>"+obj.meal_number+"</td><td><select class='foods select-temp' autocomplete='off'></select></td><td class='per-td'><input type='text' class='per' value='"+obj.weight+"'></td><td class='unit'>"+obj.unit+"</td><td class='protein'>"+obj.protein+"</td><td class='carbs'>"+obj.carbs+"</td><td class='fat'>"+obj.fat+"</td><td class='calories'>"+obj.calories+"</td><td class='remove-food-td'><span class='glyphicon glyphicon-remove cursor remove-food orange' data-meal="+obj.meal_number+"></span></td></tr>";
				}
				else
				{
					var row = "<tr class='food-row'><td></td><td><select class='foods select-temp' autocomplete='off'></select></td><td class='per-td'><input type='text' class='per' value='"+obj.weight+"'></td><td class='unit'>"+obj.unit+"</td><td class='protein'>"+obj.protein+"</td><td class='carbs'>"+obj.carbs+"</td><td class='fat'>"+obj.fat+"</td><td class='calories'>"+obj.calories+"</td><td class='remove-food-td'><span class='glyphicon glyphicon-remove cursor remove-food orange' data-meal="+obj.meal_number+"></span></td></tr>";
				}

				$("#meal-"+obj.meal_number+"-total").before(row);

				$.each(foods, function(key, value) {   
					$('.select-temp').append($("<option></option>").attr("value", foods[key].food_id).text(foods[key].food_name));
					$(".select-temp option[value='"+obj.food_id+"']").prop('selected', true);
				});
				$('.select-temp').removeClass('select-temp');

				meal_number = obj.meal_number;
			});
		}

		for(var i = 1; i <= number_of_meals; i++)
		{
			var obj = $("#meal-"+i).find('.per-td:first').find('.per');
			calculateMacros(obj);
		}

		targetGoalCalories();
	}

	// Save or update a day.
	function saveDay(day_name, day_id, duplicate) {

		var meals = [];
		var number_of_meals = $(".meal").length;

		$(".food-row").each(function(j, obj) {
			
			var temp = new Object();
			var meal_number = $(this).closest('tbody').attr('id');

			temp.meal_number = meal_number.replace('meal-', "");
			temp.food_id = $(this).find('.foods').val();
			temp.per = $(this).find('.per').val();
			temp.unit = $(this).find('.unit').html();
			temp.protein = $(this).find('.protein').html();
			temp.carbs = $(this).find('.carbs').html();
			temp.fat = $(this).find('.fat').html();
			temp.calories = $(this).find('.calories').html();
			meals.push(temp);		
		});

		$.ajax({
			type : "POST",
			url : "save-day",
			data: { "day_name" : day_name, "day_id" : day_id, "meals" : meals, "duplicate" : duplicate },
			success : function(data)
			{
			    location.reload();
			}
		})
	}

	// Delete a day.
	function deleteDay(day_id)
	{
		$.ajax({
			type : "POST",
			url : "delete-day",
			data: { "day_id" : day_id },
			success : function(data)
			{
			    location.reload();
			}
		})
	}

	function targetGoalCalories()
	{
		if($("#total-calories").html() < $("#calorie-goal").html())
		{
			$("#calorie-goal").css({ "color" : "#5cb85c" });
		}
		else
		{
			$("#calorie-goal").css({ "color" : "#f15a22" });
		}
	}
});