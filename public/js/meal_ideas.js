$( document ).ready(function() {

    var foods = [];
    var calories = 300;
    var protein_id = 50;
    var carb_id = 0;
    var fat_id = 0;
    var protein_food;
    var carbs_food = null;
    var fat_food = null;

    // Get all my foods.
    $.ajax({
        type : "GET",
        url : "get-foods",
        dataType: "json",
        success : function(data)
        {
            for(var i = 0; i < data.length; i++)
            {
                temp = new Object();
                temp.food_id = data[i]['id'];
                temp.food_name = data[i]['food_name'];
                temp.per = data[i]['per'];
                temp.unit = data[i]['unit'];
                temp.protein = data[i]['protein'];
                temp.carbs = data[i]['carbs'];
                temp.fat = data[i]['fat'];
                temp.calories = data[i]['calories'];
                temp.macro = data[i]['macro'];

                foods.push(temp);
            }

            protein_food = $.grep(foods, function(e){ return e.food_id == 50; });
        }
    })

    // Calorie range slider.
    $('input[type="range"]').rangeslider({

        // Feature detection the default is `true`.
        // Set this to `false` if you want to use
        // the polyfill also in Browsers which support
        // the native <input type="range"> element.
        polyfill: false,

        // Callback function
        onInit: function() {},

        // Callback function
        onSlide: function(position, value) {
            
            calories = value;
            $("#calories-output h2").html(value + " Calories");
        },

        // Callback function
        onSlideEnd: function(position, value) {}
    });

    // Protein radio button.
    $(".food-name-protein").change(function()
    {
        protein_id = $(this).val();
        protein_food = $.grep(foods, function(e){ return e.food_id == protein_id; });
    });

    // Carbs radio button.
    $(".food-name-carbs").change(function()
    {
        carbs_id = $(this).val();
        carbs_food = $.grep(foods, function(e){ return e.food_id == carbs_id; });
    });

    // Fat radio button.
    $(".food-name-fat").change(function()
    {
        fat_id = $(this).val();
        fat_food = $.grep(foods, function(e){ return e.food_id == fat_id; });
    });

    // Create meal button.
    $('body').on('click', '#create-meal-modal-btn', function()
    {
        createMealIdea(calories, protein_food, carbs_food, fat_food);
    });

    // Reset radio buttons.
    $('body').on('click', '#reset-radio-btn', function()
    {
        $(".food-name-carbs:checked").prop('checked', false);
        $(".food-name-fat:checked").prop('checked', false);

        carbs_food = null;
        fat_food = null;
    });

    function createMealIdea(calories, protein_food, carbs_food, fat_food)
    {
        if(carbs_food == null && fat_food == null) // Protein only meal.
        {
            $("#row-carbs").hide();
            $("#row-fat").hide();

            var p_multiplier = calories / protein_food[0]['calories'];
            var p_per = Math.floor(p_multiplier * protein_food[0]['per']);
            var p_protein = Math.floor(p_multiplier * protein_food[0]['protein']);
            var p_carbs = Math.floor(p_multiplier * protein_food[0]['carbs']);
            var p_fat = Math.floor(p_multiplier * protein_food[0]['fat']);
            var p_calories = (p_protein * 4) + (p_carbs * 4) + (p_fat * 9);

            $("#row-protein .food-name").html(protein_food[0]['food_name']);
            $("#row-protein .per").html(p_per);
            $("#row-protein .unit").html(protein_food[0]['unit']);
            $("#row-protein .protein").html(p_protein);
            $("#row-protein .carbs").html(p_carbs);
            $("#row-protein .fat").html(p_fat);
            $("#row-protein .calories").html(p_calories);

            $("#total-protein").html(p_protein);
            $("#total-carbs").html(p_carbs);
            $("#total-fat").html(p_fat);
            $("#total-calories").html(p_calories);
        }
        else if(carbs_food != null && fat_food == null) // Protein & carb meal.
        {
            $("#row-carbs").show();
            $("#row-fat").hide();

            var p_calories = calories * 0.4;
            var p_multiplier = p_calories / protein_food[0]['calories'];
            var p_per = Math.floor(p_multiplier * protein_food[0]['per']);
            var p_protein = Math.floor(p_multiplier * protein_food[0]['protein']);
            var p_carbs = Math.floor(p_multiplier * protein_food[0]['carbs']);
            var p_fat = Math.floor(p_multiplier * protein_food[0]['fat']);
            p_calories = (p_protein * 4) + (p_carbs * 4) + (p_fat * 9);

            var c_calories = calories * 0.6;
            var c_multiplier = c_calories / carbs_food[0]['calories'];
            var c_per = Math.floor(c_multiplier * carbs_food[0]['per']);
            var c_protein = Math.floor(c_multiplier * carbs_food[0]['protein']);
            var c_carbs = Math.floor(c_multiplier * carbs_food[0]['carbs']);
            var c_fat = Math.floor(c_multiplier * carbs_food[0]['fat']);
            c_calories = (c_protein * 4) + (c_carbs * 4) + (c_fat * 9);

            var total_protein = p_protein + c_protein;
            var total_carbs = p_carbs + c_carbs;
            var total_fat = p_fat + c_fat;
            var total_calories = p_calories + c_calories;

            $("#row-protein .food-name").html(protein_food[0]['food_name']);
            $("#row-protein .per").html(p_per);
            $("#row-protein .unit").html(protein_food[0]['unit']);
            $("#row-protein .protein").html(p_protein);
            $("#row-protein .carbs").html(p_carbs);
            $("#row-protein .fat").html(p_fat);
            $("#row-protein .calories").html(p_calories);

            $("#row-carbs .food-name").html(carbs_food[0]['food_name']);
            $("#row-carbs .per").html(c_per);
            $("#row-carbs .unit").html(carbs_food[0]['unit']);
            $("#row-carbs .protein").html(c_protein);
            $("#row-carbs .carbs").html(c_carbs);
            $("#row-carbs .fat").html(c_fat);
            $("#row-carbs .calories").html(c_calories);

            $("#total-protein").html(total_protein);
            $("#total-carbs").html(total_carbs);
            $("#total-fat").html(total_fat);
            $("#total-calories").html(total_calories);
        }
        else if(carbs_food == null && fat_food != null) // Protein & fat meal.
        {
            $("#row-carbs").hide();
            $("#row-fat").show();

            var p_calories = calories * 0.4;
            var p_multiplier = p_calories / protein_food[0]['calories'];
            var p_per = Math.floor(p_multiplier * protein_food[0]['per']);
            var p_protein = Math.floor(p_multiplier * protein_food[0]['protein']);
            var p_carbs = Math.floor(p_multiplier * protein_food[0]['carbs']);
            var p_fat = Math.floor(p_multiplier * protein_food[0]['fat']);
            p_calories = (p_protein * 4) + (p_carbs * 4) + (p_fat * 9);

            var f_calories = calories * 0.6;
            var f_multiplier = f_calories / fat_food[0]['calories'];
            var f_per = Math.floor(f_multiplier * fat_food[0]['per']);
            var f_protein = Math.floor(f_multiplier * fat_food[0]['protein']);
            var f_carbs = Math.floor(f_multiplier * fat_food[0]['carbs']);
            var f_fat = Math.floor(f_multiplier * fat_food[0]['fat']);
            f_calories = (f_protein * 4) + (f_carbs * 4) + (f_fat * 9);

            var total_protein = p_protein + f_protein;
            var total_carbs = p_carbs + f_carbs;
            var total_fat = p_fat + f_fat;
            var total_calories = p_calories + f_calories;

            $("#row-protein .food-name").html(protein_food[0]['food_name']);
            $("#row-protein .per").html(p_per);
            $("#row-protein .unit").html(protein_food[0]['unit']);
            $("#row-protein .protein").html(p_protein);
            $("#row-protein .carbs").html(p_carbs);
            $("#row-protein .fat").html(p_fat);
            $("#row-protein .calories").html(p_calories);

            $("#row-fat .food-name").html(fat_food[0]['food_name']);
            $("#row-fat .per").html(f_per);
            $("#row-fat .unit").html(fat_food[0]['unit']);
            $("#row-fat .protein").html(f_protein);
            $("#row-fat .carbs").html(f_carbs);
            $("#row-fat .fat").html(f_fat);
            $("#row-fat .calories").html(f_calories);

            $("#total-protein").html(total_protein);
            $("#total-carbs").html(total_carbs);
            $("#total-fat").html(total_fat);
            $("#total-calories").html(total_calories);
        }
        else if(carbs_food != null && fat_food != null) // Protein, carb & fat meal.
        {
            $("#row-carbs").show();
            $("#row-fat").show();

            var p_calories = calories * 0.4;
            var p_multiplier = p_calories / protein_food[0]['calories'];
            var p_per = Math.floor(p_multiplier * 100);
            var p_protein = Math.floor(p_multiplier * protein_food[0]['protein']);
            var p_carbs = Math.floor(p_multiplier * protein_food[0]['carbs']);
            var p_fat = Math.floor(p_multiplier * protein_food[0]['fat']);
            p_calories = (p_protein * 4) + (p_carbs * 4) + (p_fat * 9);

            var c_calories = calories * 0.4;
            var c_multiplier = c_calories / carbs_food[0]['calories'];
            var c_per = Math.floor(c_multiplier * carbs_food[0]['per']);
            var c_protein = Math.floor(c_multiplier * carbs_food[0]['protein']);
            var c_carbs = Math.floor(c_multiplier * carbs_food[0]['carbs']);
            var c_fat = Math.floor(c_multiplier * carbs_food[0]['fat']);
            c_calories = (c_protein * 4) + (c_carbs * 4) + (c_fat * 9);

            var f_calories = calories * 0.2;
            var f_multiplier = f_calories / fat_food[0]['calories'];
            var f_per = Math.floor(f_multiplier * fat_food[0]['per']);
            var f_protein = Math.floor(f_multiplier * fat_food[0]['protein']);
            var f_carbs = Math.floor(f_multiplier * fat_food[0]['carbs']);
            var f_fat = Math.floor(f_multiplier * fat_food[0]['fat']);
            f_calories = (f_protein * 4) + (f_carbs * 4) + (f_fat * 9);

            var total_protein = p_protein + c_protein + f_protein;
            var total_carbs = p_carbs + c_carbs + f_carbs;
            var total_fat = p_fat + c_fat + f_fat;
            var total_calories = p_calories + c_calories + f_calories;

            $("#row-protein .food-name").html(protein_food[0]['food_name']);
            $("#row-protein .per").html(p_per);
            $("#row-protein .unit").html(protein_food[0]['unit']);
            $("#row-protein .protein").html(p_protein);
            $("#row-protein .carbs").html(p_carbs);
            $("#row-protein .fat").html(p_fat);
            $("#row-protein .calories").html(p_calories);

            $("#row-carbs .food-name").html(carbs_food[0]['food_name']);
            $("#row-carbs .per").html(c_per);
            $("#row-carbs .unit").html(carbs_food[0]['unit']);
            $("#row-carbs .protein").html(c_protein);
            $("#row-carbs .carbs").html(c_carbs);
            $("#row-carbs .fat").html(c_fat);
            $("#row-carbs .calories").html(c_calories);

            $("#row-fat .food-name").html(fat_food[0]['food_name']);
            $("#row-fat .per").html(f_per);
            $("#row-fat .unit").html(fat_food[0]['unit']);
            $("#row-fat .protein").html(f_protein);
            $("#row-fat .carbs").html(f_carbs);
            $("#row-fat .fat").html(f_fat);
            $("#row-fat .calories").html(f_calories);

            $("#total-protein").html(total_protein);
            $("#total-carbs").html(total_carbs);
            $("#total-fat").html(total_fat);
            $("#total-calories").html(total_calories);
        }
    }
});

