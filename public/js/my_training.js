$( document ).ready(function() {

	// Add a new exercise.
	$("body").on('click', '#add-exercise', function()
	{
        var row = "<tr class='exercise-row'><td><input type='text' class='exercise-name'></td><td><select class='sets'><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='5'>5</option><option value='6'>6</option><option value='7'>7</option><option value='8'>8</option><option value='9'>9</option><option value='10'>10</option></select></td><td><input type='text' class='rep-range'></td><td><input type='text' class='rest-period'></td><td><input type='text' class='notes'></td><td><span class='glyphicon glyphicon-remove cursor remove-exercise orange'></span></td></tr>";
        $(this).closest('tr.add-exercise-tr').before(row);
        $('#save-session-modal-btn').attr("disabled", false);
    });

    // Delete an exercise.
	$("body").on('click', '.remove-exercise', function()
    {
		$(this).closest('tr').remove();
	});

	// Opens the save session modal.
	$("#save-session-modal-btn").click(function()
	{
		var session_name_old = $("#my-training-days-select option:selected").val();

		if(session_name_old != '-')
		{
			$("#session-name").attr('placeholder', session_name_old);
		}
		else
		{
			$("#session-name").attr('placeholder', '');
		}
	});

	
});