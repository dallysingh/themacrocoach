$( document ).ready(function() {

	$(".add-food-db").click(function()
	{
		if(!$(this).hasClass('success'))
		{
			$(this).removeClass('glyphicon-plus orange cursor');

			$(this).parent().parent().addClass('food-added');
			$(this).addClass('glyphicon glyphicon-ok success');
			$(this).css({'color' : '#5cb85c'});

			var food_id = $(this).closest('tr').find('.food-name').data('food-id');
			var food_name = $(this).closest('tr').find('.food-name').html();
			var per = $(this).closest('tr').find('.food-per').html();
			var unit = $(this).closest('tr').find('.food-unit').html();
			var protein = $(this).closest('tr').find('.food-protein').html();
			var carbs = $(this).closest('tr').find('.food-carbs').html();
			var fat = $(this).closest('tr').find('.food-fat').html();
			var calories = $(this).closest('tr').find('.food-calories').html();

			$.ajax({
				type : "POST",
				url : "add-food-db",
				data: { "food_id" : food_id, "food_name" : food_name, "per" : per, "unit" : unit, "protein" : protein, "carbs" : carbs, "fat" : fat, "calories" : calories },
				success : function(data)
				{
				    
				}
			})
		}
	});

});