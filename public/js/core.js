$( document ).ready(function() {

	// Active & unactive states for the header navigation links.
	$(".nav a").each(function()
	{
		if(window.location.href.indexOf($(this).attr("href")) >= 0 && $(this).attr("href") != "")
		{
			$(".nav li").removeClass("active");
			$(this).closest('li').addClass("active");
		}
	});

});