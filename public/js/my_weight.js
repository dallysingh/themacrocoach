$( document ).ready(function() {

	var kg = 0;
	var lbs = 0;
	var height;
	var dob;
	var gender;
	var bmi = 0;
	var bmr = 0;
	var today = new Date();

	$("#weight-goal-kg").numeric();
	$("#weight-goal-lbs").numeric();

	$('.datepicker').datepicker({
		format: 'dd/mm/yyyy',
		endDate: '+0d',
		weekStart: 1
	});

	$.ajax({
	    type : "GET",
	    url : "get-my-profile",
	    data : { },
	    success : function(data)
	    {
	    	height = data[0]['height_cm'];
	    	dob = data[0]['dob'];
	    	gender = data[0]['gender']
	    }
	});

	// Create the weight line chart.
	if($('#my-weight-chart').length)
	{
	    var array_date = new Array();
		var array_weight_kg = new Array();
		var array_weight_lbs = new Array();

	    $.ajax({
		    type : "GET",
		    url : "get-my-weight",
		    data : { },
		    success : function(data)
		    {
		    	for(var i = data.length - 1; i >= 0; i--)
		    	{
		    		var date = data[i]['date'];
		    		date = moment(date).format('DD MMM');
		    				    		
		    		array_date[data.length - (i+1)] = date;
		    		array_weight_kg[data.length - (i+1)] = parseFloat(data[i]['kg']);
		    		array_weight_lbs[data.length - (i+1)] = parseFloat(data[i]['lbs']);
				}

				var data = {
			    labels: array_date,
			    datasets: [
			    		{
				            label: "KG",
				            fill: false,
				            lineTension: 0.1,
				            backgroundColor: "rgba(241, 90, 34, 0.4)",
				            borderColor: "rgba(226, 75, 19, 1)",
				            borderCapStyle: 'butt',
				            borderDash: [],
				            borderDashOffset: 0.0,
				            borderJoinStyle: 'miter',
				            pointBorderColor: "rgba(226, 75, 19, 1)",
				            pointBackgroundColor: "rgba(241, 90, 34, 0.4)",
				            pointBorderWidth: 5,
				            pointHoverRadius: 7,
				            pointHoverBackgroundColor: "rgba(241, 90, 34, 1)",
				            pointHoverBorderColor: "rgba(226, 75, 19, 1)",
				            pointHoverBorderWidth: 2,
				            pointRadius: 1,
				            pointHitRadius: 10,
				            data: array_weight_kg,
				            spanGaps: false,
				        },
			        	{
				            label: "LBS",
				            fill: false,
				            lineTension: 0.1,
				            backgroundColor: "rgba(204, 204, 204, 0.5)",
				            borderColor: "rgba(204, 204, 204, 1)",
				            borderCapStyle: 'butt',
				            borderDash: [],
				            borderDashOffset: 0.0,
				            borderJoinStyle: 'miter',
				            pointBorderColor: "rgba(204, 204, 204, 1)",
				            pointBackgroundColor: "rgba(204, 204, 204, 0.5)",
				            pointBorderWidth: 5,
				            pointHoverRadius: 7,
				            pointHoverBackgroundColor: "rgba(204, 204, 204, 1)",
				            pointHoverBorderColor: "rgba(204, 204, 204, 1)",
				            pointHoverBorderWidth: 2,
				            pointRadius: 1,
				            pointHitRadius: 10,
				            data: array_weight_lbs,
				            spanGaps: false,
			        	}
				    ]
				};

				var options = {
				    scales: {
				        yAxes: [{
				            display: true,
				            ticks: {
				                beginAtZero: true
				            }
				        }]
				    }
				};

				var ctx = document.getElementById("my-weight-chart").getContext("2d");
				var my_weight_chart = new Chart(ctx, {
										    type: 'line',
										    data: data,
										    options: options,
										    responsive: true,
										    maintainAspectRatio: false,
										});
			}
		});
	}

	$("#weight-goal-kg").bind("keyup keydown", function()
	{
		kg = $(this).val();

		lbs = convertKG(kg);
		calculateBMIandBMR(kg, height, gender, dob);

		$("#weight-goal-lbs").val(lbs);
		$("#weight-goal-bmi").html(bmi);
		$("#weight-goal-bmr").html(bmr);
	});

	$("#weight-goal-lbs").bind("keyup keydown", function()
	{
		lbs = $(this).val();

		kg = convertLBS(lbs);
		calculateBMIandBMR(kg, height, gender, dob);

		$("#weight-goal-kg").val(kg);
		$("#weight-goal-bmi").html(bmi);
		$("#weight-goal-bmr").html(bmr);
	});

	$("#add-weight-kg").bind("keyup keydown", function()
	{
		kg = $(this).val();

		lbs = convertKG(kg);
		calculateBMIandBMR(kg, height, gender, dob);

		$("#add-weight-lbs").val(lbs);
		$("#add-weight-bmi").html(bmi);
		$("#add-weight-bmr").html(bmr);
	});

	$("#add-weight-lbs").bind("keyup keydown", function()
	{
		lbs = $(this).val();

		kg = convertLBS(lbs);
		calculateBMIandBMR(kg, height, gender, dob);

		$("#add-weight-kg").val(kg);
		$("#add-weight-bmi").html(bmi);
		$("#add-weight-bmr").html(bmr);
	});

	$("#edit-weight-kg").bind("keyup keydown", function()
	{
		kg = $(this).val();

		lbs = convertKG(kg);
		calculateBMIandBMR(kg, height, gender, dob);

		$("#edit-weight-lbs").val(lbs);
		$("#edit-weight-bmi").html(bmi);
		$("#edit-weight-bmr").html(bmr);
	});

	$("#edit-weight-lbs").bind("keyup keydown", function()
	{
		lbs = $(this).val();

		kg = convertLBS(lbs);
		calculateBMIandBMR(kg, height, gender, dob);

		$("#edit-weight-kg").val(kg);
		$("#edit-weight-bmi").html(bmi);
		$("#edit-weight-bmr").html(bmr);
	});

	$("#set-weight-goal-btn").click(function()
	{
		var weight_goal_kg = $("#weight-goal-kg").val();
		var weight_goal_lbs = $("#weight-goal-lbs").val();
		var weight_goal_bmi = $("#weight-goal-bmi").html();
		var weight_goal_bmr = $("#weight-goal-bmr").html();

		if(weight_goal_kg.length == 0 || weight_goal_lbs.length == 0)
		{
			$("#weight-goal-kg").addClass("empty-field");
			$("#weight-goal-lbs").addClass("empty-field");
		}
		else
		{
			$.ajax({
			    type : "POST",
			    url : "set-weight-goal",
			    data : { "kg" : weight_goal_kg, "lbs" : weight_goal_lbs, "bmi" : weight_goal_bmi, "bmr" : weight_goal_bmr },
			    success : function(data)
			    {
			    	location.reload();
			    }
			});
		}
	});

	$("#add-weight-btn").click(function()
	{
		var add_weight_date = $("#add-weight-date").val();
		var add_weight_kg = $("#add-weight-kg").val();
		var add_weight_lbs = $("#add-weight-lbs").val();
		var add_weight_bmi = $("#add-weight-bmi").html();
		var add_weight_bmr = $("#add-weight-bmr").html();

		if(add_weight_kg.length == 0 || add_weight_lbs.length == 0)
		{
			$("#add-weight-kg").addClass("empty-field");
			$("#add-weight-lbs").addClass("empty-field");

			if(add_weight_date.length == 0)
			{
				$("#add-weight-date").addClass("empty-field");
			}
		}
		else if(add_weight_date.length == 0)
		{
			$("#add-weight-date").addClass("empty-field");
		}
		else
		{
			$.ajax({
			    type : "POST",
			    url : "add-weight",
			    data : { "date" : add_weight_date, "kg" : add_weight_kg, "lbs" : add_weight_lbs, "bmi" : add_weight_bmi, "bmr" : add_weight_bmr },
			    success : function(data)
			    {
			    	location.reload();
			    }
			});
		}
	});

	$(".edit-weight").click(function()
	{
		var weight_id = $(this).closest('tr').find('.weight-date').data('weight-id');
		var edit_date = $(this).closest('tr').find('.weight-date').html();
		var edit_weight_kg = $(this).closest('tr').find('.weight-kg').html();
		var edit_weight_lbs = $(this).closest('tr').find('.weight-lbs').html();
		var edit_weight_bmi = $(this).closest('tr').find('.weight-bmi').html();
		var edit_weight_bmr = $(this).closest('tr').find('.weight-bmr').html();

		$("#weight_id").val(weight_id);
		$("#edit-weight-date").attr('placeholder', edit_date);
		$("#edit-weight-date").val(edit_date);
		$("#edit-weight-kg").val(edit_weight_kg);
		$("#edit-weight-lbs").val(edit_weight_lbs);
		$("#edit-weight-bmi").html(edit_weight_bmi);
		$("#edit-weight-bmr").html(edit_weight_bmr);
	});

	$("#update-weight-btn").click(function()
	{
		var weight_id = $("#weight_id").val();
		var edit_weight_date = $("#edit-weight-date").val();
		if(edit_weight_date.length == 0) { edit_weight_date = $("#edit-weight-date").attr('placeholder'); }
		var edit_weight_kg = $("#edit-weight-kg").val();
		var edit_weight_lbs = $("#edit-weight-lbs").val();
		var edit_weight_bmi = $("#edit-weight-bmi").html();
		var edit_weight_bmr = $("#edit-weight-bmr").html();

		if(edit_weight_kg.length == 0 || edit_weight_lbs.length == 0)
		{
			$("#edit-weight-kg").addClass("empty-field");
			$("#edit-weight-lbs").addClass("empty-field");

			if(edit_weight_date.length == 0)
			{
				$("#edit-weight-date").addClass("empty-field");
			}
		}
		else if(edit_weight_date.length == 0)
		{
			$("#edit-weight-date").addClass("empty-field");
		}
		else
		{
			$.ajax({
			    type : "POST",
			    url : "edit-weight",
			    data : { "weight_id" : weight_id, "date" : edit_weight_date, "kg" : edit_weight_kg, "lbs" : edit_weight_lbs, "bmi" : edit_weight_bmi, "bmr" : edit_weight_bmr },
			    success : function(data)
			    {
			    	location.reload();
			    }
			});
		}
	});

	$("#delete-weight-btn").click(function()
	{
		var weight_id = $("#weight_id").val();

		$.ajax({
			    type : "POST",
			    url : "delete-weight",
			    data : { "weight_id" : weight_id },
			    success : function(data)
			    {
			    	location.reload();
			    }
			});
	});

	// Calculate a persons BMI & BMR.
	function calculateBMIandBMR(kg, cm, gender, date_of_birth)
	{
		bmi = ((kg / ((cm / 100) * (cm / 100)))).toFixed(1);
		var age = today.getYear() - new Date(date_of_birth).getYear() - 1;
		
		if(kg > 0 && gender == "male")
		{
			// Harris-Benedict Formula
			// bmr = (66 + (13.7 * kg) + (5 * cm) - (6.8 * age)).toFixed(1);

			// Mifflin St Jeor Formula
			bmr = ((10 * kg) + (6.25 * cm) - (5 * age) + 5).toFixed(1);
		}
		else if(kg > 0 && gender == "female")
		{
			// Harris-Benedict Formula
			// bmr = (655 + (9.6 * kg) + (1.8 * cm) - (4.7 * age)).toFixed(1);

			// Mifflin St Jeor Formula
			bmr = ((10 * kg) + (6.25 * cm) - (5 * age) - 161).toFixed(1);
		}
		else
		{
			bmr = 0;
		}
	}

	// Convert kg to lbs.
	function convertKG(kg)
	{
		lbs = (kg * 2.2046).toFixed(1);
		return lbs;
	}

	// Convert lbs to kg.
	function convertLBS(lbs)
	{
		kg = (lbs / 2.2046).toFixed(1);
		return kg;
	}

});

