$( document ).ready(function() {

    var bmr = $("#bmr").html();
    var calorie_intake = $("#calorie-intake").html();
    var weight_data;

    $.ajax({
        type : "GET",
        url : "get-my-current-weight",
        dataType: "json",
        success : function(data)
        {
            weight_data = data;
        }
    })

    // BMR range slider.
    $('#bmr-range-slider').rangeslider({

        polyfill: false,

        // Callback function
        onInit: function() {},

        // Callback function
        onSlide: function(position, value) {
            
            if(value == 1) { maintenance_calories = weight_data[0]['bmr']; $("#bmr-output h2").html("<span id='activity-title'>Rest <span id='activity-desc'>No exercise or activity</span><span id='bmr'>" + parseFloat(maintenance_calories).toFixed(1) + "</span> Calories"); }
            if(value == 2) { maintenance_calories = weight_data[0]['bmr'] * 1.2; $("#bmr-output h2").html("<span id='activity-title'>Sedentary</span> <span id='activity-desc'>Little or no exercise</span><span id='bmr'>" + parseFloat(maintenance_calories).toFixed(1) + " Calories"); }
            if(value == 3) { maintenance_calories = weight_data[0]['bmr'] * 1.375; $("#bmr-output h2").html("<span id='activity-title'>Lightly Active</span> <span id='activity-desc'>Light exercise or sports 1-3 days per week</span><span id='bmr'>" + parseFloat(maintenance_calories).toFixed(1) + "</span> Calories"); }
            if(value == 4) { maintenance_calories = weight_data[0]['bmr'] * 1.55; $("#bmr-output h2").html("<span id='activity-title'>Moderately Active</span> <span id='activity-desc'>Moderate exercise or sports 3-5 days per week</span><span id='bmr'>" + parseFloat(maintenance_calories).toFixed(1) + "</span> Calories"); }
            if(value == 5) { maintenance_calories = weight_data[0]['bmr'] * 1.725; $("#bmr-output h2").html("<span id='activity-title'>Very Active</span> <span id='activity-desc'>Hard exercise or sports 6-7 days per week</span><span id='bmr'>" + parseFloat(maintenance_calories).toFixed(1) + "</span> Calories"); }
            if(value == 6) { maintenance_calories = weight_data[0]['bmr'] * 1.9; $("#bmr-output h2").html("<span id='activity-title'>Extremely Active</span> <span id='activity-desc'>Very hard exercise or sports multiple times a day</span><span id='bmr'>" + parseFloat(maintenance_calories).toFixed(1) + "</span> Calories"); }
            
            var calorie_calculation = parseFloat($("#calories-output h2").html()).toFixed(1) - maintenance_calories;
            if(calorie_calculation < 0)
            {
                $("#calorie-calculation-output h2").html(calorie_calculation.toFixed(1) + " Calorie Deficit");
            }
            else
            {
                $("#calorie-calculation-output h2").html(calorie_calculation.toFixed(1) + " Calorie Surplus");
            }
        },

        // Callback function
        onSlideEnd: function(position, value) {

            var activity_title = $("#activity-title").html();
            var activity_description = $("#activity-desc").html();

            $.ajax({
                type : "POST",
                url : "set-activity",
                data : { "activity_level" : value, "activity_title" : activity_title, "activity_description" : activity_description, "maintenance_calories" : maintenance_calories },
                success : function(data)
                {
                    
                }
            })
        }
    });

    // Calorie range slider.
    $('#calorie-range-slider').rangeslider({

        polyfill: false,

        // Callback function
        onInit: function() {},

        // Callback function
        onSlide: function(position, value) {
            
            $("#calories-output h2").html(value + " Calories");

            var calorie_calculation = value - parseFloat($("#bmr").html()).toFixed(1);

            if(bmr > value)
            {
                $("#calorie-calculation-output h2").html(calorie_calculation.toFixed(1) + " Calorie Deficit");
            }
            else
            {
                $("#calorie-calculation-output h2").html(calorie_calculation.toFixed(1) + " Calorie Surplus");
            }
        },

        // Callback function
        onSlideEnd: function(position, value) {

            $.ajax({
                type : "POST",
                url : "set-calorie-intake",
                data : { "calorie_intake" : value },
                success : function(data)
                {
                    
                }
            })
        }
    });

});

